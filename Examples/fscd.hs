{-# LANGUAGE NoMonomorphismRestriction #-}

import Language.FUnCAL 
import Control.Monad
import Data.Monoid

nthM :: Functor m => Int -> m [a] -> m a
nthM i m = (!! i) <$> m 

ctail =
  nthM 0 .
  gfoldG 2 (\a [x,y] -> sequence [return y, a <:> return y])



input1 = fixG (\x -> 1 <:> 2 <:> return x)

isHeadA t =
  not <$> 
   (isEmptyG =<< 
    forG (\a x -> if a == "A" then "dummy" <:> end
                  else             end) t)

hasAA t = do
  r <- gfoldG 2 (\a [x,y] -> sequence $
                             if a == "A" then [ return y, "dummy" <:> end ]
                             else             [ return x,  end ] ) t
  b <- isEmptyG $ head r
  return $ not b 

input2 = fixG (\x -> "A" <:> return x)
input3 = "B" <:> "A" <:> "A" <:> "B" <:> end 

res1 = testF ctail input1 

input4 = gfixG 3 (\[a,b,c] ->
                   sequence[ mconcat [ "name" <:> "alice" <:> end,
                                       "knows" <:> return c ],
                             mconcat [ "name" <:> "bob" <:> end,
                                       "knows" <:> return a ],
                             mconcat [ "name" <:> "carol" <:> end,
                                       "knows" <:> (return a <> return b) ] ])

collect = paraG (\a g x -> if a == "name"       then "nm" <:> return g 
                           else if a == "knows" then return x
                                else                 end)

res21 = testF isHeadA input2
res22 = testF isHeadA input3
res23 = testF hasAA   input2
res24 = testF hasAA   input3

res3  = testF collect (nthM 0 input4)

testF f t = do
  x <- t
  r <- f x
  putStrLn "Input:"
  print x
  putStrLn "\nOutput:" 
  print r
  
{-
*Main> res1 
Inserted entry:  526 -> [527,528]
Input:
Root:
  @526
Heap:
  @526: 	1 : @529
  @529: 	2 : @526


Output:
Inserted entry:  529 -> [531,532]
Lookup succeeds: 526 -> [527,528]
Lookup succeeds: 529 -> [531,532]
Root:
  @527
Heap:
  @527: 	2 : @528
  @528: 	1 : @532
  @532: 	2 : @528

*Main> res21
Inserted entry:  536 -> [537]
Lookup succeeds: 536 -> [537]
Input:
Root:
  @536
Heap:
  @536: 	"A" : @536


Output:
True
*Main> res22
Inserted entry:  543 -> [544]
Inserted entry:  542 -> [545]
Input:
Root:
  @543
Heap:
  @543: 	"B" : @542
  @542: 	"A" : @541
  @541: 	"A" : @540
  @540: 	"B" : *


Output:
False
*Main> res23
Inserted entry:  546 -> [547,548]
Lookup succeeds: 546 -> [547,548]
Lookup succeeds: 546 -> [547,548]
Input:
Root:
  @546
Heap:
  @546: 	"A" : @546


Output:
True
*Main> res24
Inserted entry:  555 -> [556,557]
Inserted entry:  554 -> [558,559]
Inserted entry:  553 -> [560,561]
Inserted entry:  552 -> [563,564]
Input:
Root:
  @555
Heap:
  @555: 	"B" : @554
  @554: 	"A" : @553
  @553: 	"A" : @552
  @552: 	"B" : *


Output:
True

*Main> res3
Inserted entry:  566 -> [569]
Input:
Root:
  @566
Heap:
  @566: 	@571 U @573
  @573: 	@572 U *
  @572: 	"knows" : @568
  @568: 	@597 U @600
  @600: 	@599 U *
  @599: 	"knows" : @598
  @598: 	@566 U @567
  @567: 	@608 U @610
  @610: 	@609 U *
  @609: 	"knows" : @566
  @608: 	"name" : @607
  @607: 	"bob" : *
  @597: 	"name" : @596
  @596: 	"carol" : *
  @571: 	"name" : @570
  @570: 	"alice" : *


Output:
Inserted entry:  571 -> [618]
Inserted entry:  573 -> [619]
Inserted entry:  570 -> [621]
Inserted entry:  572 -> [623]
Inserted entry:  568 -> [625]
Inserted entry:  597 -> [626]
Inserted entry:  600 -> [627]
Inserted entry:  596 -> [629]
Inserted entry:  599 -> [631]
Inserted entry:  598 -> [633]
Lookup succeeds: 566 -> [569]
Inserted entry:  567 -> [634]
Inserted entry:  608 -> [636]
Inserted entry:  610 -> [637]
Inserted entry:  607 -> [639]
Inserted entry:  609 -> [641]
Lookup succeeds: 566 -> [569]
Root:
  @569
Heap:
  @569: 	@618 U @619
  @619: 	@623 U *
  @623: 	@626 U @627
  @627: 	@631 U *
  @631: 	@569 U @634
  @634: 	@636 U @637
  @637: 	@641 U *
  @641: 	@618 U @619
  @636: 	"nm" : @607
  @607: 	"bob" : *
  @626: 	"nm" : @596
  @596: 	"carol" : *
  @618: 	"nm" : @570
  @570: 	"alice" : *

*Main> (collect =<< nthM 0 input4) >>= toGraph
Inserted entry:  643 -> [646]
Inserted entry:  648 -> [663]
Inserted entry:  650 -> [664]
Inserted entry:  647 -> [666]
Inserted entry:  649 -> [668]
Inserted entry:  645 -> [670]
Inserted entry:  682 -> [687]
Inserted entry:  685 -> [688]
Inserted entry:  681 -> [690]
Inserted entry:  684 -> [692]
Inserted entry:  683 -> [694]
Lookup succeeds: 643 -> [646]
Inserted entry:  644 -> [695]
Inserted entry:  703 -> [713]
Inserted entry:  705 -> [714]
Inserted entry:  702 -> [716]
Inserted entry:  704 -> [718]
Lookup succeeds: 643 -> [646]
R: 1
E: 0,
   1 ---> 2,
     ---> 3,
   2 --[ "nm" ]-> 4,
   3 ---> 6,
     ---> 0,
   4 --[ "alice" ]-> 0,
   6 ---> 8,
     ---> 9,
   8 --[ "nm" ]-> 10,
   9 ---> 12,
     ---> 0,
   10 --[ "carol" ]-> 0,
   12 ---> 1,
      ---> 15,
   15 ---> 16,
      ---> 17,
   16 --[ "nm" ]-> 18,
   17 ---> 20,
      ---> 0,
   18 --[ "bob" ]-> 0,
   20 ---> 2,
      ---> 3
*Main> (collect =<< nthM 0 input4) >>= toGraph'
Inserted entry:  720 -> [723]
Inserted entry:  725 -> [740]
Inserted entry:  727 -> [741]
Inserted entry:  724 -> [743]
Inserted entry:  726 -> [745]
Inserted entry:  722 -> [747]
Inserted entry:  759 -> [764]
Inserted entry:  762 -> [765]
Inserted entry:  758 -> [767]
Inserted entry:  761 -> [769]
Inserted entry:  760 -> [771]
Lookup succeeds: 720 -> [723]
Inserted entry:  721 -> [772]
Inserted entry:  780 -> [790]
Inserted entry:  782 -> [791]
Inserted entry:  779 -> [793]
Inserted entry:  781 -> [795]
Lookup succeeds: 720 -> [723]
fromList [0,1,4,10,18]
R: 1
E: 1 --[ "nm" ]-> 18,
     --[ "nm" ]-> 10,
     --[ "nm" ]-> 4,
   4 --[ "alice" ]-> 0,
   10 --[ "carol" ]-> 0,
   18 --[ "bob" ]-> 0

-}
