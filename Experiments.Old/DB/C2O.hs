{-# LANGUAGE NoMonomorphismRestriction #-}
module DB.C2O where 

import Language.FUnCAL

import Data.List
import Control.Monad 
import Util

db :: Memo (G Memo Label)                            
db =
       (nthM 7) (gfixG 8 (\ t156 ->
                            return
                              [unionsG [(LConst "type") >: (LConst "shipping") >: end,
                                        (LConst "code") >: (LString "200-777") >: end,
                                        (LConst "info") >: (LString "BiG office of Tokyo") >: end],
                               unionsG [(LConst "type") >: (LConst "contractual") >: end,
                                        (LConst "code") >: (LString "100-888") >: end,
                                        (LConst "info") >: (LString "IPL of Tokyo") >: end],
                               unionsG [(LConst "name") >: (LString "Tanaka") >: end,
                                        (LConst "email") >: (LString "tanaka@biglab") >: end,
                                        (LConst "email") >: (LString "tanaka@gmail") >: end,
                                        (LConst "add") >: ((nthM 0) t156),
                                        (LConst "order") >: ((nthM 4) t156),
                                        (LConst "order") >: ((nthM 5) t156)],
                               unionsG [(LConst "name") >: (LString "Kato") >: end,
                                        (LConst "email") >: (LString "kato@biglab") >: end,
                                        (LConst "add") >: ((nthM 0) t156),
                                        (LConst "add") >: ((nthM 1) t156),
                                        (LConst "order") >: ((nthM 6) t156)],
                               unionsG [(LConst "date") >: (LString "16/07/2008") >: end,
                                        (LConst "no") >: (LInt 1001) >: end,
                                        (LConst "order_of") >: ((nthM 2) t156)],
                               unionsG [(LConst "date") >: (LString "16/10/2008") >: end,
                                        (LConst "no") >: (LInt 1002) >: end,
                                        (LConst "order_of") >: ((nthM 2) t156)],
                               unionsG [(LConst "date") >: (LString "16/12/2008") >: end,
                                        (LConst "no") >: (LInt 1003) >: end,
                                        (LConst "order_of") >: ((nthM 3) t156)],
                               unionsG [(LConst "customer") >: ((nthM 2) t156),
                                        (LConst "customer") >: ((nthM 3) t156)]]))

query :: LazyG Memo Label -> LazyG Memo Label 
query xdb = 
    paraG (\ xL xg t125 ->
             if xL == (LConst "order") then
               (paraG (\ xL xn t15 ->
                         if xL == (LConst "customer_name") then
                           (paraG (\ xnameL xAny t7 ->
                                     if xnameL == (LString "Tanaka") then
                                       (LConst "order") >: xg
                                     else
                                       end)) xn
                         else
                           end)) xg
             else
               end) ((paraG (\ xL xfv3 t119 ->
                               if xL == (LConst "customer") then
                                 (paraG (\ xL xo t111 ->
                                           if xL == (LConst "order") then
                                             (paraG (\ xL xc t103 ->
                                                       if xL == (LConst "order_of") then
                                                         (paraG (\ xL xdate t95 ->
                                                                   if xL == (LConst "date") then
                                                                     (paraG (\ xL xno t87 ->
                                                                               if xL == (LConst "no") then
                                                                                 (paraG (\ xL xa t79 ->
                                                                                           if xL == (LConst "add") then
                                                                                             (paraG (\ xL xname t71 ->
                                                                                                       if xL == (LConst "name") then
                                                                                                         (paraG (\ xL xcode t63 ->
                                                                                                                   if xL == (LConst "code") then
                                                                                                                     (paraG (\ xL xinfo t55 ->
                                                                                                                               if xL == (LConst "info") then
                                                                                                                                 (paraG (\ xL xfv1 t47 ->
                                                                                                                                           if xL == (LConst "type") then
                                                                                                                                             (paraG (\ xL xanyv2 t39 ->
                                                                                                                                                       if xL == (LConst "shipping") then
                                                                                                                                                         (LConst "order") >: (unionsG [(LConst "date") >: xdate,
                                                                                                                                                                                       (LConst "no") >: xno,
                                                                                                                                                                                       (LConst "customer_name") >: xname,
                                                                                                                                                                                       (LConst "addr") >: xa])
                                                                                                                                                       else
                                                                                                                                                         end)) xfv1
                                                                                                                                           else
                                                                                                                                             end)) xa
                                                                                                                               else
                                                                                                                                 end)) xa
                                                                                                                   else
                                                                                                                     end)) xa
                                                                                                       else
                                                                                                         end)) xc
                                                                                           else
                                                                                             end)) xc
                                                                               else
                                                                                 end)) xo
                                                                   else
                                                                     end)) xo
                                                       else
                                                         end)) xo
                                           else
                                             end)) xfv3
                               else
                                 end)) xdb)


query2 :: (t -> LazyG Memo Label) -> t -> LazyG Memo Label
query2 xdb = 
    (\ t140 ->
       paraG (\ xL t137 ->
                (\ xg t139 ->
                   if xL == (LConst "order") then
                     paraG (\ xL t15 t17 ->
                              if xL == (LConst "customer_name") then
                                (paraG (\ xnameL t6 t8 ->
                                          if xnameL == (LString "Tanaka") then
                                            (LConst "order") >: (xg [])
                                          else
                                            end)) t15
                              else
                                end) (xg [])
                   else
                     end) (\ t138 -> t137)) (paraG (\ xL t131 t133 ->
                                                      if xL == (LConst "customer") then
                                                        (paraG (\ xL t122 ->
                                                                  (\ xo t124 ->
                                                                     if xL == (LConst "order") then
                                                                       paraG (\ xL t113 ->
                                                                                (\ xc t115 ->
                                                                                   if xL == (LConst "order_of") then
                                                                                     paraG (\ xL t104 t106 ->
                                                                                              if xL == (LConst "date") then
                                                                                                paraG (\ xL t95 t97 ->
                                                                                                         if xL == (LConst "no") then
                                                                                                           paraG (\ xL t86 ->
                                                                                                                    (\ xa t88 ->
                                                                                                                       if xL == (LConst "add") then
                                                                                                                         paraG (\ xL t77 t79 ->
                                                                                                                                  if xL == (LConst "name") then
                                                                                                                                    paraG (\ xL t68 t70 ->
                                                                                                                                             if xL == (LConst "code") then
                                                                                                                                               paraG (\ xL t59 t61 ->
                                                                                                                                                        if xL == (LConst "info") then
                                                                                                                                                          paraG (\ xL t50 t52 ->
                                                                                                                                                                   if xL == (LConst "type") then
                                                                                                                                                                     (paraG (\ xL t41 t43 ->
                                                                                                                                                                               if xL == (LConst "shipping") then
                                                                                                                                                                                 (LConst "order") >: (unionsG [(LConst "date") >: t104,
                                                                                                                                                                                                               (LConst "no") >: t95,
                                                                                                                                                                                                               (LConst "customer_name") >: t77,
                                                                                                                                                                                                               (LConst "addr") >: (xa [])])
                                                                                                                                                                               else
                                                                                                                                                                                 end)) t50
                                                                                                                                                                   else
                                                                                                                                                                     end) (xa [])
                                                                                                                                                        else
                                                                                                                                                          end) (xa [])
                                                                                                                                             else
                                                                                                                                               end) (xa [])
                                                                                                                                  else
                                                                                                                                    end) (xc [])
                                                                                                                       else
                                                                                                                         end) (\ t87 ->
                                                                                                                                 t86)) (xc [])
                                                                                                         else
                                                                                                           end) (xo [])
                                                                                              else
                                                                                                end) (xo [])
                                                                                   else
                                                                                     end) (\ t114 ->
                                                                                             t113)) (xo [])
                                                                     else
                                                                       end) (\ t123 -> t122))) t131
                                                      else
                                                        end) (xdb t140)))
