{-# LANGUAGE NoMonomorphismRestriction #-}
module DB.PIM2PSM where 

import Language.FUnCAL

import Data.List
import Control.Monad 
import Util

db :: LazyG Memo Label 
db =
       (nthM 9) (gfixG 10 (\ t149 ->
                             return
                               [(LConst "CustomerAddress") >: (unionsG [(LConst "type") >: (LConst "String") >: (LString "Office") >: end,
                                                                        (LConst "maddRef") >: ((nthM 4) t149)]),
                                (LConst "CustomerAddress") >: (unionsG [(LConst "type") >: (LConst "String") >: (LString "Shipping") >: end,
                                                                        (LConst "maddRef") >: ((nthM 5) t149)]),
                                (LConst "Customer") >: (unionsG [(LConst "name") >: (LConst "String") >: (LString "Tanaka") >: end,
                                                                 (LConst "addRef") >: ((nthM 0) t149),
                                                                 (LConst "orderRef") >: ((nthM 6) t149)]),
                                (LConst "Customer") >: (unionsG [(LConst "name") >: (LConst "String") >: (LString "Sato") >: end,
                                                                 (LConst "addRef") >: ((nthM 0) t149),
                                                                 (LConst "addRef") >: ((nthM 1) t149),
                                                                 (LConst "orderRef") >: ((nthM 7) t149),
                                                                 (LConst "orderRef") >: ((nthM 8) t149)]),
                                (LConst "MasterAddress") >: (unionsG [(LConst "street") >: (LConst "String") >: (LString "Hongo 7-3-1") >: end,
                                                                      (LConst "city") >: (LConst "String") >: (LString "Tokyo") >: end,
                                                                      (LConst "zip") >: (LConst "String") >: (LString "113-8656") >: end]),
                                (LConst "MasterAddress") >: (unionsG [(LConst "street") >: (LConst "String") >: (LString "Waseda 1-1-1") >: end,
                                                                      (LConst "city") >: (LConst "String") >: (LString "Tokyo") >: end,
                                                                      (LConst "zip") >: (LConst "String") >: (LString "169-0051") >: end]),
                                (LConst "CustomerOrder") >: (LConst "number") >: (LConst "String") >: (LString "1111") >: end,
                                (LConst "CustomerOrder") >: (LConst "number") >: (LConst "String") >: (LString "2222") >: end,
                                (LConst "CustomerOrder") >: (LConst "number") >: (LConst "String") >: (LString "3333") >: end,
                                unionsG [(LConst "top") >: ((nthM 2) t149),
                                         (LConst "top") >: ((nthM 3) t149)]]))

query :: LazyG Memo Label -> LazyG Memo Label 
query xdb = 
    paraG (\ xL xfv43 t373 ->
             if xL == (LConst "top") then
               (paraG (\ xL xfv44 t15 ->
                         if xL == (LConst "Customer") then
                           (paraG (\ xL xorder t7 ->
                                     if xL == (LConst "orderRef") then
                                       (LConst "top") >: xorder
                                     else
                                       end)) xfv44
                         else
                           end)) xfv43
             else
               end) (paraG (\ xL36 xcustomer t367 ->
                              if xL36 == (LConst "top") then
                                xL36 >: ((paraG (\ xL38 xfv1 t67 ->
                                                   if xL38 == (LConst "Customer") then
                                                     xL38 >: ((paraG (\ xL40 xorder t57 ->
                                                                        if xL40 == (LConst "orderRef") then
                                                                          ifM
                                                                          (isEmptyG ((paraG (\ xL xorderInf t26 ->
                                                                                               if xL == (LConst "CustomerOrder") then
                                                                                                 (LConst "orderRef") >: end
                                                                                               else
                                                                                                 end)) xorder))
                                                                          (xL40 >: xorder)
                                                                          (xL40 >: ((paraG (\ xL xorderInf t45 ->
                                                                                              if xL == (LConst "CustomerOrder") then
                                                                                                (LConst "Order") >: (unionsG [unionsG [(LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end,
                                                                                                                                       (LConst "customerRef") >: xcustomer],
                                                                                                                              xorderInf])
                                                                                              else
                                                                                                end)) xorder))
                                                                        else
                                                                          xL40 >: xorder)) xfv1)
                                                   else
                                                     xL38 >: xfv1)) xcustomer)
                              else
                                xL36 >: xcustomer) (paraG (\ xL28 xfv25 t361 ->
                                                             if xL28 == (LConst "top") then
                                                               xL28 >: ((paraG (\ xL30 xfv26 t185 ->
                                                                                  if xL30 == (LConst "Customer") then
                                                                                    xL30 >: ((paraG (\ xL32 xadd t175 ->
                                                                                                       if xL32 == (LConst "addRef") then
                                                                                                         ifM
                                                                                                         (isEmptyG ((paraG (\ xL xfv4 t112 ->
                                                                                                                              if xL == (LConst "CustomerAddress") then
                                                                                                                                (paraG (\ xL xtype t104 ->
                                                                                                                                          if xL == (LConst "type") then
                                                                                                                                            (paraG (\ xL xfv2 t96 ->
                                                                                                                                                      if xL == (LConst "CustomerAddress") then
                                                                                                                                                        (paraG (\ xL xfv3 t88 ->
                                                                                                                                                                  if xL == (LConst "maddRef") then
                                                                                                                                                                    (paraG (\ xL xmadd t80 ->
                                                                                                                                                                              if xL == (LConst "MasterAddress") then
                                                                                                                                                                                (LConst "addRef") >: end
                                                                                                                                                                              else
                                                                                                                                                                                end)) xfv3
                                                                                                                                                                  else
                                                                                                                                                                    end)) xfv2
                                                                                                                                                      else
                                                                                                                                                        end)) xadd
                                                                                                                                          else
                                                                                                                                            end)) xfv4
                                                                                                                              else
                                                                                                                                end)) xadd))
                                                                                                         (xL32 >: xadd)
                                                                                                         (xL32 >: ((paraG (\ xL xfv4 t163 ->
                                                                                                                             if xL == (LConst "CustomerAddress") then
                                                                                                                               (paraG (\ xL xtype t155 ->
                                                                                                                                         if xL == (LConst "type") then
                                                                                                                                           (paraG (\ xL xfv2 t147 ->
                                                                                                                                                     if xL == (LConst "CustomerAddress") then
                                                                                                                                                       (paraG (\ xL xfv3 t139 ->
                                                                                                                                                                 if xL == (LConst "maddRef") then
                                                                                                                                                                   (paraG (\ xL xmadd t131 ->
                                                                                                                                                                             if xL == (LConst "MasterAddress") then
                                                                                                                                                                               (LConst "Address") >: (unionsG [(LConst "type") >: xtype,
                                                                                                                                                                                                               (LConst "stereotype") >: (LConst "String") >: (LString "simpleType") >: end,
                                                                                                                                                                                                               xmadd])
                                                                                                                                                                             else
                                                                                                                                                                               end)) xfv3
                                                                                                                                                                 else
                                                                                                                                                                   end)) xfv2
                                                                                                                                                     else
                                                                                                                                                       end)) xadd
                                                                                                                                         else
                                                                                                                                           end)) xfv4
                                                                                                                             else
                                                                                                                               end)) xadd))
                                                                                                       else
                                                                                                         xL32 >: xadd)) xfv26)
                                                                                  else
                                                                                    xL30 >: xfv26)) xfv25)
                                                             else
                                                               xL28 >: xfv25) (paraG (\ xL20 xfv18 t355 ->
                                                                                        if xL20 == (LConst "top") then
                                                                                          xL20 >: ((paraG (\ xL22 xcustomer2 t262 ->
                                                                                                             if xL22 == (LConst "Customer") then
                                                                                                               ifM
                                                                                                               (isEmptyG ((paraG (\ xL xfv5 t214 ->
                                                                                                                                    if xL == (LConst "name") then
                                                                                                                                      (paraG (\ xL xfv6 t206 ->
                                                                                                                                                if xL == (LConst "String") then
                                                                                                                                                  (paraG (\ xL xanyv7 t198 ->
                                                                                                                                                            if xL == (LString "Sato") then
                                                                                                                                                              (LConst "Customer") >: end
                                                                                                                                                            else
                                                                                                                                                              end)) xfv6
                                                                                                                                                else
                                                                                                                                                  end)) xfv5
                                                                                                                                    else
                                                                                                                                      end)) xcustomer2))
                                                                                                               (xL22 >: xcustomer2)
                                                                                                               (xL22 >: ((paraG (\ xL xfv5 t250 ->
                                                                                                                                   if xL == (LConst "name") then
                                                                                                                                     (paraG (\ xL xfv6 t242 ->
                                                                                                                                               if xL == (LConst "String") then
                                                                                                                                                 (paraG (\ xL xanyv7 t234 ->
                                                                                                                                                           if xL == (LString "Sato") then
                                                                                                                                                             unionsG [xcustomer2,
                                                                                                                                                                      (LConst "id") >: (LConst "Integer") >: (LInt 2) >: end,
                                                                                                                                                                      (LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end]
                                                                                                                                                           else
                                                                                                                                                             end)) xfv6
                                                                                                                                               else
                                                                                                                                                 end)) xfv5
                                                                                                                                   else
                                                                                                                                     end)) xcustomer2))
                                                                                                             else
                                                                                                               xL22 >: xcustomer2)) xfv18)
                                                                                        else
                                                                                          xL20 >: xfv18) ((paraG (\ xL13 xfv11 t349 ->
                                                                                                                    if xL13 == (LConst "top") then
                                                                                                                      xL13 >: ((paraG (\ xL15 xcustomer1 t339 ->
                                                                                                                                         if xL15 == (LConst "Customer") then
                                                                                                                                           ifM
                                                                                                                                           (isEmptyG ((paraG (\ xL xfv8 t291 ->
                                                                                                                                                                if xL == (LConst "name") then
                                                                                                                                                                  (paraG (\ xL xfv9 t283 ->
                                                                                                                                                                            if xL == (LConst "String") then
                                                                                                                                                                              (paraG (\ xL xanyv10 t275 ->
                                                                                                                                                                                        if xL == (LString "Tanaka") then
                                                                                                                                                                                          (LConst "Customer") >: end
                                                                                                                                                                                        else
                                                                                                                                                                                          end)) xfv9
                                                                                                                                                                            else
                                                                                                                                                                              end)) xfv8
                                                                                                                                                                else
                                                                                                                                                                  end)) xcustomer1))
                                                                                                                                           (xL15 >: xcustomer1)
                                                                                                                                           (xL15 >: ((paraG (\ xL xfv8 t327 ->
                                                                                                                                                               if xL == (LConst "name") then
                                                                                                                                                                 (paraG (\ xL xfv9 t319 ->
                                                                                                                                                                           if xL == (LConst "String") then
                                                                                                                                                                             (paraG (\ xL xanyv10 t311 ->
                                                                                                                                                                                       if xL == (LString "Tanaka") then
                                                                                                                                                                                         unionsG [xcustomer1,
                                                                                                                                                                                                  (LConst "id") >: (LConst "Integer") >: (LInt 1) >: end,
                                                                                                                                                                                                  (LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end]
                                                                                                                                                                                       else
                                                                                                                                                                                         end)) xfv9
                                                                                                                                                                           else
                                                                                                                                                                             end)) xfv8
                                                                                                                                                               else
                                                                                                                                                                 end)) xcustomer1))
                                                                                                                                         else
                                                                                                                                           xL15 >: xcustomer1)) xfv11)
                                                                                                                    else
                                                                                                                      xL13 >: xfv11)) xdb))))


query2 :: (t -> LazyG Memo Label) -> t -> LazyG Memo Label 
query2 xdb =
    (\ t411 ->
       paraG (\ xL t408 t410 ->
                if xL == (LConst "top") then
                  (paraG (\ xL t15 t17 ->
                            if xL == (LConst "Customer") then
                              (paraG (\ xL t6 t8 ->
                                        if xL == (LConst "orderRef") then
                                          (LConst "top") >: t6
                                        else
                                          end)) t15
                            else
                              end)) t408
                else
                  end) (paraG (\ xL36 t402 ->
                                 (\ xcustomer t404 ->
                                    if xL36 == (LConst "top") then
                                      xL36 >: (paraG (\ xL38 t72 ->
                                                        (\ xfv1 t74 ->
                                                           if xL38 == (LConst "Customer") then
                                                             xL38 >: (paraG (\ xL40 t62 ->
                                                                               (\ xorder t64 ->
                                                                                  if xL40 == (LConst "orderRef") then
                                                                                    ifM
                                                                                    (isEmptyG (paraG (\ xL t29 t31 ->
                                                                                                        if xL == (LConst "CustomerOrder") then
                                                                                                          (LConst "orderRef") >: end
                                                                                                        else
                                                                                                          end) (xorder [])))
                                                                                    (xL40 >: (xorder []))
                                                                                    (xL40 >: (paraG (\ xL t50 t52 ->
                                                                                                       if xL == (LConst "CustomerOrder") then
                                                                                                         (LConst "Order") >: (unionsG [unionsG [(LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end,
                                                                                                                                                (LConst "customerRef") >: (xcustomer [])],
                                                                                                                                       t50])
                                                                                                       else
                                                                                                         end) (xorder [])))
                                                                                  else
                                                                                    xL40 >: (xorder [])) (\ t63 ->
                                                                                                            t62)) (xfv1 []))
                                                           else
                                                             xL38 >: (xfv1 [])) (\ t73 ->
                                                                                   t72)) (xcustomer []))
                                    else
                                      xL36 >: (xcustomer [])) (\ t403 ->
                                                                 t402)) (paraG (\ xL28 t396 ->
                                                                                  (\ xfv25 t398 ->
                                                                                     if xL28 == (LConst "top") then
                                                                                       xL28 >: (paraG (\ xL30 t202 ->
                                                                                                         (\ xfv26 t204 ->
                                                                                                            if xL30 == (LConst "Customer") then
                                                                                                              xL30 >: (paraG (\ xL32 t192 ->
                                                                                                                                (\ xadd t194 ->
                                                                                                                                   if xL32 == (LConst "addRef") then
                                                                                                                                     ifM
                                                                                                                                     (isEmptyG (paraG (\ xL t123 t125 ->
                                                                                                                                                         if xL == (LConst "CustomerAddress") then
                                                                                                                                                           (paraG (\ xL t114 t116 ->
                                                                                                                                                                     if xL == (LConst "type") then
                                                                                                                                                                       paraG (\ xL t105 t107 ->
                                                                                                                                                                                if xL == (LConst "CustomerAddress") then
                                                                                                                                                                                  (paraG (\ xL t96 t98 ->
                                                                                                                                                                                            if xL == (LConst "maddRef") then
                                                                                                                                                                                              (paraG (\ xL t87 t89 ->
                                                                                                                                                                                                        if xL == (LConst "MasterAddress") then
                                                                                                                                                                                                          (LConst "addRef") >: end
                                                                                                                                                                                                        else
                                                                                                                                                                                                          end)) t96
                                                                                                                                                                                            else
                                                                                                                                                                                              end)) t105
                                                                                                                                                                                else
                                                                                                                                                                                  end) (xadd [])
                                                                                                                                                                     else
                                                                                                                                                                       end)) t123
                                                                                                                                                         else
                                                                                                                                                           end) (xadd [])))
                                                                                                                                     (xL32 >: (xadd []))
                                                                                                                                     (xL32 >: (paraG (\ xL t180 t182 ->
                                                                                                                                                        if xL == (LConst "CustomerAddress") then
                                                                                                                                                          (paraG (\ xL t171 t173 ->
                                                                                                                                                                    if xL == (LConst "type") then
                                                                                                                                                                      paraG (\ xL t162 t164 ->
                                                                                                                                                                               if xL == (LConst "CustomerAddress") then
                                                                                                                                                                                 (paraG (\ xL t153 t155 ->
                                                                                                                                                                                           if xL == (LConst "maddRef") then
                                                                                                                                                                                             (paraG (\ xL t144 t146 ->
                                                                                                                                                                                                       if xL == (LConst "MasterAddress") then
                                                                                                                                                                                                         (LConst "Address") >: (unionsG [(LConst "type") >: t171,
                                                                                                                                                                                                                                         (LConst "stereotype") >: (LConst "String") >: (LString "simpleType") >: end,
                                                                                                                                                                                                                                         t144])
                                                                                                                                                                                                       else
                                                                                                                                                                                                         end)) t153
                                                                                                                                                                                           else
                                                                                                                                                                                             end)) t162
                                                                                                                                                                               else
                                                                                                                                                                                 end) (xadd [])
                                                                                                                                                                    else
                                                                                                                                                                      end)) t180
                                                                                                                                                        else
                                                                                                                                                          end) (xadd [])))
                                                                                                                                   else
                                                                                                                                     xL32 >: (xadd [])) (\ t193 ->
                                                                                                                                                           t192)) (xfv26 []))
                                                                                                            else
                                                                                                              xL30 >: (xfv26 [])) (\ t203 ->
                                                                                                                                     t202)) (xfv25 []))
                                                                                     else
                                                                                       xL28 >: (xfv25 [])) (\ t397 ->
                                                                                                              t396)) (paraG (\ xL20 t390 ->
                                                                                                                               (\ xfv18 t392 ->
                                                                                                                                  if xL20 == (LConst "top") then
                                                                                                                                    xL20 >: (paraG (\ xL22 t288 ->
                                                                                                                                                      (\ xcustomer2 t290 ->
                                                                                                                                                         if xL22 == (LConst "Customer") then
                                                                                                                                                           ifM
                                                                                                                                                           (isEmptyG (paraG (\ xL t235 t237 ->
                                                                                                                                                                               if xL == (LConst "name") then
                                                                                                                                                                                 (paraG (\ xL t226 t228 ->
                                                                                                                                                                                           if xL == (LConst "String") then
                                                                                                                                                                                             (paraG (\ xL t217 t219 ->
                                                                                                                                                                                                       if xL == (LString "Sato") then
                                                                                                                                                                                                         (LConst "Customer") >: end
                                                                                                                                                                                                       else
                                                                                                                                                                                                         end)) t226
                                                                                                                                                                                           else
                                                                                                                                                                                             end)) t235
                                                                                                                                                                               else
                                                                                                                                                                                 end) (xcustomer2 [])))
                                                                                                                                                           (xL22 >: (xcustomer2 []))
                                                                                                                                                           (xL22 >: (paraG (\ xL t276 t278 ->
                                                                                                                                                                              if xL == (LConst "name") then
                                                                                                                                                                                (paraG (\ xL t267 t269 ->
                                                                                                                                                                                          if xL == (LConst "String") then
                                                                                                                                                                                            (paraG (\ xL t258 t260 ->
                                                                                                                                                                                                      if xL == (LString "Sato") then
                                                                                                                                                                                                        unionsG [xcustomer2 [],
                                                                                                                                                                                                                 (LConst "id") >: (LConst "Integer") >: (LInt 2) >: end,
                                                                                                                                                                                                                 (LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end]
                                                                                                                                                                                                      else
                                                                                                                                                                                                        end)) t267
                                                                                                                                                                                          else
                                                                                                                                                                                            end)) t276
                                                                                                                                                                              else
                                                                                                                                                                                end) (xcustomer2 [])))
                                                                                                                                                         else
                                                                                                                                                           xL22 >: (xcustomer2 [])) (\ t289 ->
                                                                                                                                                                                       t288)) (xfv18 []))
                                                                                                                                  else
                                                                                                                                    xL20 >: (xfv18 [])) (\ t391 ->
                                                                                                                                                           t390)) (paraG (\ xL13 t384 ->
                                                                                                                                                                            (\ xfv11 t386 ->
                                                                                                                                                                               if xL13 == (LConst "top") then
                                                                                                                                                                                 xL13 >: (paraG (\ xL15 t374 ->
                                                                                                                                                                                                   (\ xcustomer1 t376 ->
                                                                                                                                                                                                      if xL15 == (LConst "Customer") then
                                                                                                                                                                                                        ifM
                                                                                                                                                                                                        (isEmptyG (paraG (\ xL t321 t323 ->
                                                                                                                                                                                                                            if xL == (LConst "name") then
                                                                                                                                                                                                                              (paraG (\ xL t312 t314 ->
                                                                                                                                                                                                                                        if xL == (LConst "String") then
                                                                                                                                                                                                                                          (paraG (\ xL t303 t305 ->
                                                                                                                                                                                                                                                    if xL == (LString "Tanaka") then
                                                                                                                                                                                                                                                      (LConst "Customer") >: end
                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                      end)) t312
                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                          end)) t321
                                                                                                                                                                                                                            else
                                                                                                                                                                                                                              end) (xcustomer1 [])))
                                                                                                                                                                                                        (xL15 >: (xcustomer1 []))
                                                                                                                                                                                                        (xL15 >: (paraG (\ xL t362 t364 ->
                                                                                                                                                                                                                           if xL == (LConst "name") then
                                                                                                                                                                                                                             (paraG (\ xL t353 t355 ->
                                                                                                                                                                                                                                       if xL == (LConst "String") then
                                                                                                                                                                                                                                         (paraG (\ xL t344 t346 ->
                                                                                                                                                                                                                                                   if xL == (LString "Tanaka") then
                                                                                                                                                                                                                                                     unionsG [xcustomer1 [],
                                                                                                                                                                                                                                                              (LConst "id") >: (LConst "Integer") >: (LInt 1) >: end,
                                                                                                                                                                                                                                                              (LConst "stereotype") >: (LConst "String") >: (LString "complexType") >: end]
                                                                                                                                                                                                                                                   else
                                                                                                                                                                                                                                                     end)) t353
                                                                                                                                                                                                                                       else
                                                                                                                                                                                                                                         end)) t362
                                                                                                                                                                                                                           else
                                                                                                                                                                                                                             end) (xcustomer1 [])))
                                                                                                                                                                                                      else
                                                                                                                                                                                                        xL15 >: (xcustomer1 [])) (\ t375 ->
                                                                                                                                                                                                                                    t374)) (xfv11 []))
                                                                                                                                                                               else
                                                                                                                                                                                 xL13 >: (xfv11 [])) (\ t385 ->
                                                                                                                                                                                                        t384)) (xdb t411))))))
