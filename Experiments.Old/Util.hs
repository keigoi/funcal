module Util where

import System.CPUTime 
import System.IO 

import Control.DeepSeq

data Label = LConst  String 
           | LString String 
           | LInt    Int
           | LBool   Bool 
             deriving (Eq,Ord) 

instance Show Label where 
    show (LConst s)  = s 
    show (LString s) = show s 
    show (LInt i)    = show i
    show (LBool l)   = show l 

instance NFData Label where
    rnf (LConst s)  = rnf s 
    rnf (LString s) = rnf s 
    rnf (LInt i)    = rnf i 
    rnf (LBool l)   = rnf l 


speedTest :: NFData a => a -> IO Double 
speedTest f =         
  do startTime <- getCPUTime 
     print $ rnf f 
     endTime <- getCPUTime 
     let elapsed = fromIntegral (endTime - startTime) / 10^9 
     putStrLn $ "# Elapsed: " ++ show elapsed ++ " ms"
     return elapsed 

speedTests :: NFData a => (b -> a) -> [b] -> IO Double
speedTests f bs =
    do startTime <- getCPUTime
       print $ rnf [ f i | i <- bs ]
       endTime <- getCPUTime
       let elapsed = fromIntegral (endTime - startTime) / 10^9 
       putStrLn $ "# Total:   " ++ show elapsed ++ " ms" 
       putStrLn $ "# Avarage: " ++ show (elapsed / fromIntegral (length bs)) ++ " ms"
       return elapsed 
