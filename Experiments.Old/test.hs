{-# LANGUAGE NoMonomorphismRestriction, FlexibleInstances  #-}

import Language.FUnCAL

import System.CPUTime 
import System.IO 

import Control.DeepSeq

import Util

import DB.Class6
import DB.C2O
import DB.PIM2PSM

import Data.Char
import Debug.Trace 

type GM a = G Memo a 

instance NFData a => NFData (G Memo a) where 
    rnf x = rnf $ runMemo $ toGraph (return x)

eval x = runMemo $ toGraph' x
evalRaw x = runMemo $ toGraph x 

a2d_xc :: Memo (GM Label) -> Memo (GM Label)
a2d_xc = foldG (\a r -> if a == LConst "a" then 
                            LConst "d" >: r 
                        else if a == LConst "c" then 
                                 r 
                             else
                                 a >: r)
-- elim :: GrEx String -> GrEx String 
elim = foldG (\a r -> r)

-- consecutive :: GrEx String -> GrEx String 
-- consecutive = forG (\a g -> 
--                         forG (\b g' ->
--                                   if a == b then g'
--                                   else           end 
--                              ) g)

aloop :: Int -> Memo (G Memo Label)
aloop k = head =<< 
    gfixG k (\m -> do { x <- m 
                      ; return $ map (\y -> LConst "a" >: y) (tail x ++ [head x])})

aseq :: Int -> Memo (G Memo Label) 
aseq k = head =<<
         gfixG k (\m -> do { x <- m
                           ; return $ map (\y -> LConst "a" >: y) (tail x)})

-- genChar c =
--     let modulo = ord 'z' - ord 'a' + 1 in
--     let ccode  = ord 'a' + (c `mod` modulo) in
--     chr ccode

-- genLabel c = Lab [genChar (fromIntegral c)]

-- genLatticeLikeGraph level = 
--     if level < 1 then Graph [0] [] 0 else
--         if level == 1 then 
--             Graph [1] [] 1 
--         else if level == 2 then 
--                  Graph [1,2,3] [(1,Lab "a", 2), (1,Lab "b",3)] 1
--              else
--                 let (Graph v e r) = genLatticeLikeGraph (level -1) in 
--                 let (vS, eS) = child (level - 1) in 
--                 (Graph (vS++v) (eS++e)  r)                 
--     where
--       s l = ( ( l * (l - 1)) `div` 2 ) + 1
--       rev (u,l,v) = (v,l,u)
--       child k = 
--           if k <= 0 then ([], [])
--           else 
--               let (vS,eS) = child (k-1) in 
--               let lc = s level + k - 1 in
--               let p  = lc - level + 1  in
--               let rc = lc + 1 in
--               let lln = lc + k - 2 in 
--               let rln = lln + 1 in
--               let lb = (p, genLabel lln, lc) in
--               let rb = (rc, genLabel rln, p) in 
--               let (lb',rb') =
--                       if level `mod` 2 == 1 then 
--                           (rev lb, rev rb)
--                       else
--                           (lb, rb) in 
--               (lc:rc:vS, lb':rb':eS)
                             
                   
-- rotateL n l = drop n l ++ take n l 

-- latOld k = fromGraph $ eval $ 
--       fmap head $ 
--          gfixG (k*k) (\x -> sequence $ 
--                           zipWith (\a b -> unionsG ["A" >: return a, "A" >: return b]) (rotateL k x) (rotateL 1 x))


latRaw :: Int -> Graph Label 
latRaw k = 
    Graph vs 
          ([ (v,Lab $ LConst "A", (v+1) `mod` (k*k)) | v <- vs ]
           ++
           [ (v,Lab $ LConst "A", (v+k) `mod` (k*k)) | v <- vs ])
          (head vs)
    where
      vs = [0..k*k-1]

completeG :: Int -> Graph Label
completeG k =
    Graph vs
          [ (v, Lab $ LConst "A", u) | v <- vs, u <- vs, v /= u ]
          (head vs)
    where
      vs = [0 .. k]

lat :: Int -> Memo (G Memo Label) 
lat k = fromGraph $ latRaw k 
          
sizeG (Graph v e r) = (length v, length e)

latRaw' k = 
    Graph vs 
          ([ (v,Lab $ LConst "A", (v+1)) | v <- vs,  v+1 < k*k ]
           ++
           [ (v,Lab $ LConst "A", (v+k)) | v <- vs,  v+k < k*k ])
          (head vs)
    where
      vs = [0..k*k-1]


-- asource k = 
--   head $ 
--   gfixG k (\x ->
--             map (\r -> 
--                   case r `mod` 4 of 
--                     0 -> unionsG ["A" >: x !! adjust (r+1), "C" >: x !! adjust (r-2) ]
--                     1 -> "B" >: x !! adjust (r+1)
--                     2 -> unionsG ["C" >: x !! adjust (r+1), "C" >: x !! adjust (r-2) ]
--                     3 -> "D" >: x !! adjust (r+1)
--                 ) 
--             [0..k-1])
--   where 
--     adjust r = (k + r) `mod` k
    
    
-- main = do speedTest $ query (\_ -> customers) undefined
-- main = do speedTest $ query (\_ -> pim_db1) undefined 

qTest :: NFData a => (Memo (GM a) -> Memo (GM a)) -> Memo (GM a)
                     -> Integer -> IO Double 
qTest q i n = do speedTest $ runMemo i
                 speedTests (\_ -> runMemo $ q i ) [1..n]

putD s = do putStrLn $ s
            putStrLn $ replicate (length s) '-'

seqTest k n = do putD $ "aseq " ++ show k 
                 let g = aseq k 
                 qTest a2d_xc g n 
   
latTest k n = do putD $ "lat " ++ show k 
                 let g = lat k 
                 qTest a2d_xc g n 

comTest k n = do putD $ "complete " ++ show k 
                 let g = fromGraph $ completeG k 
                 qTest a2d_xc g n 

c2oTest n = do putD $ "C2O_Sel"
               let g = DB.C2O.db 
               qTest DB.C2O.query g n 


class6Test n = do putD $ "Class6"
                  let g = DB.Class6.db  
                  qTest DB.Class6.query g n 

pim2psmTest n = do putD $ "PIM2PSM"
                   let g = DB.PIM2PSM.db 
                   qTest DB.PIM2PSM.query g n 

mytest = do { seqTest 30000 5
            ; latTest 200   5
            ; comTest 200   5 
            ; c2oTest 10
            ; class6Test 10 
            ; pim2psmTest 10 
            }

main = mytest 
--           putStrLn "seq 10000"
--           let v = aseq 10000
--           speedTest v 
--           speedTest $ a2d_xc v
--           putStrLn "C2O"
--           speedTest customers 
--           speedTest $ DB.C2O.query (\_ -> customers) undefined 
--           putStrLn "PIM2PSM"
--           speedTest pim_db1
--           speedTest $ DB.PIM2PSM.query (\_ -> pim_db1) undefined 

-- main = print $ asource 40
