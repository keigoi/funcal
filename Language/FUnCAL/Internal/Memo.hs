{- |
Monads powerful enough to implementation memoized lazy evaluation. 
-}

{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ExistentialQuantification #-}

{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE TypeApplications #-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Language.FUnCAL.Internal.Memo where


import Data.STRef
import Data.IORef
import Control.Monad.ST
import Control.Monad.State 
import Control.Monad.Identity

import System.IO.Unsafe
import Unsafe.Coerce 

import Data.Coerce
import Data.IntMap as IM 

-- | A monad for memoization.
--   Parameter @m@ is assumeted to be an instance of MonadIO.
newtype MemoIO m a = MemoIO { runMemoIO :: m a } 
        deriving (Functor, Applicative, Monad, MonadFix)

-- | A type class for memoized computation 
class (Monad m, Functor m) => Memoizing m where
  data MemoRef m a
  newRef   :: a -> m (MemoRef m a)
  writeRef :: MemoRef m a -> a -> m ()
  readRef  :: MemoRef m a -> m a 
  newUniq  :: m Int 


-- | Slightly generalized implementation. Type @t@ of 'MemoST' t s a'
--   is assumed to be an instance of 'MonadTrans', where @t (ST s)@
--   must be a monad.

newtype MemoST' t s a = MemoST' (STRef s Int -> t (ST s) a)

instance (Functor (t (ST s))) => Functor (MemoST' t s) where
  fmap f (MemoST' k) = coerce (\x -> f <$> k x)

instance Applicative (t (ST s)) => Applicative (MemoST' t s) where
  pure x = MemoST' (\_ -> pure x)
  MemoST' f <*> MemoST' a = MemoST' $ \s ->
    f s <*> a s

instance (Monad (t (ST s))) => Monad (MemoST' t s) where
  return x = MemoST' (\_ -> return x)
  MemoST' m >>= f = MemoST' $ \ref -> do
    x <- m ref
    coerce (f x) ref 

instance (MonadTrans t, Monad (t (ST s))) => Memoizing (MemoST' t s) where
  newtype MemoRef (MemoST' t s) a = MemoSTRef' (STRef s a)
  newRef x = MemoST' $ \_ -> fmap MemoSTRef' $ lift $ newSTRef x

  writeRef r v = MemoST' $ \_ ->
    lift $ writeSTRef (coerce r) v

  readRef r = MemoST' $ \_ ->
    lift $ readSTRef (coerce r)

  newUniq = MemoST' $ \ref -> do 
    lift $ modifySTRef' ref (+1)
    v <- lift $ readSTRef ref 
    v `seq` return v 

instance MonadFix (t (ST s)) => MonadFix (MemoST' t s) where
  mfix f = MemoST' $ \ref -> mfix (\a -> let MemoST' k = f a in k ref)

  
-- | A monad for memoization using ST. 
newtype MemoST s a = MemoST (STRef s Int -> ST s a)

instance Functor (MemoST s) where
  fmap f (MemoST k) = coerce (\x -> f <$> k x)

instance Applicative (MemoST s) where
  pure = return
  (<*>) = ap

instance Monad (MemoST s) where
  return x = MemoST (\_ -> return x)
  MemoST m >>= f = MemoST $ \ref -> do
    x <- m ref
    coerce (f x) ref 

instance Memoizing (MemoST s)  where
  newtype MemoRef (MemoST s) a = MemoSTRef (STRef s a)
  newRef x       = MemoST $ \_ -> coerce $ newSTRef x 

  writeRef ref !x = MemoST $ \_ -> do
    writeSTRef (coerce ref) x
    return ()

  readRef ref    = MemoST $ \_ -> readSTRef (coerce ref)

  newUniq        = MemoST $ \uref -> do
    modifySTRef' uref (+1)
    i <- readSTRef uref 
    i `seq` return i 

instance MonadFix (MemoST s) where
  mfix f = MemoST $ \ref -> mfix (\a -> let MemoST k = f a in k ref)

runMemoST :: (forall s. MemoST s a) -> a
runMemoST m = runST (newSTRef 0 >>= (let MemoST k = m in k)) 

                                                      
instance Memoizing IO where
  newtype MemoRef IO a = MemoIORef (IORef a)
  {-# INLINE newRef #-}
  newRef   = coerce . newIORef
  {-# INLINE writeRef #-}
  writeRef = writeIORef . coerce 
  {-# INLINE readRef #-}
  readRef  = readIORef . coerce 
  {-# INLINE newUniq #-}
  newUniq  = myNewUnique 

instance MonadIO m => Memoizing (MemoIO m) where
  newtype MemoRef (MemoIO m) a = MemoMemoRef (IORef a) 

  {-# INLINE newRef #-}
  newRef r     = MemoIO $ liftIO $ coerce $ newIORef r 
  {-# INLINE writeRef #-}
  writeRef r v = MemoIO $ liftIO $ writeIORef (coerce r) v  
  {-# INLINE readRef #-}
  readRef r    = MemoIO $ liftIO $ readIORef (coerce r)
  {-# INLINE newUniq #-}
  newUniq      = MemoIO $ liftIO myNewUnique 

myuniqRef :: IORef Int 
myuniqRef = unsafePerformIO $ newIORef 0 
{-# NOINLINE myuniqRef #-}

myNewUnique :: IO Int 
myNewUnique = do
  r <- atomicModifyIORef' myuniqRef $ \x -> let !z = x+1 in (z,z)
  r `seq` return r  

data Store s = Store { count  :: !Int ,
                       uniq   :: !Int , 
                       table  :: !(IM.IntMap Untyped) }

data Untyped = forall a. Untyped a

typed :: Untyped -> a
typed (Untyped a) = unsafeCoerce a 

-- | Pure implementation
-- 
--   Warning: this implementation is memory-leaking.
--   Please use 'MemoST' if you want to perform graph transformation
--   in a pure context.
--   To compose effects, using 'MemoST'' or 'MemoIO' would be preferrable.

type PMemoT s = StateT (Store s)
type PMemo  s = PMemoT s Identity 

instance Monad m => Memoizing (StateT (Store s) m) where
  newtype MemoRef (StateT (Store s) m) a = MemoStateRef Int

  newRef r = StateT $ \ store@Store { count=c, table=t } ->
    return (coerce c, store { count = c + 1,
                              table = IM.insert c (Untyped r) t })

  writeRef k v = StateT $ \ store@Store {table = t} ->
    return ((),
            store { table = IM.insert (coerce k) (Untyped v) t })

  readRef k = StateT $ \ store@Store {table=t} -> do
    Just v <- return $ IM.lookup (coerce k) t
    return (typed v, store)

  newUniq = StateT $ \ store@Store {uniq=u} ->
    return (u, store { uniq = u + 1 })
    

runPMemoT :: Monad m => (forall s. PMemoT s m a) -> m a
runPMemoT m = 
  evalStateT m (Store 1 1 IM.empty)

runMemo :: (forall m. (Memoizing m, MonadFix m) => m a) -> a
runMemo m = runMemoST m 
             

data Loc m a =
  Loc { locID  :: {-# UNPACK #-} !Int,  -- ^ unique identifier 
        locRef :: !(MemoRef m a) }      -- ^ actual reference

instance Show (Loc m a) where
  show l = "@" ++ show (locID l)

-- | Thunk.
--   Type constructor m of @Thunk m a@ is assummed to be an instance of @Memoizing@. 
data Thunk m a = Suspended !(m a)
               | Evaluated !a

{-# INLINABLE newLoc #-}
newLoc :: Memoizing m => a -> m (Loc m a)
newLoc !a = do
  r <- newRef a
  u <- newUniq
  return $ Loc u r

{-# INLINABLE readLoc #-}
readLoc :: Memoizing m => Loc m a -> m a
readLoc loc = readRef (locRef loc)

{-# INLINABLE writeLoc #-}
writeLoc :: Memoizing m => Loc m a -> a -> m ()
writeLoc loc = (writeRef (locRef loc) $!)

-- Usual lazy evaluation 
{-# INLINABLE resolveLoc #-}
resolveLoc :: Memoizing m => Loc m (Thunk m a) -> m a
resolveLoc loc = do
  let r = locRef loc 
  th <- readRef r
  case th of
    Evaluated v -> return v
    Suspended m   -> do
      v <- m
      writeRef r $! Evaluated v
      return v 

