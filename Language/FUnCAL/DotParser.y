{
module Language.FUnCAL.DotParser where

import Language.FUnCAL.DotSyntax
import Language.FUnCAL.DotLexer
}

%name pGraph Graph
%tokentype { Token }
%monad     { Alex }
%lexer     { lexer } { EOF } 
%error     { parseError }


%token
  ":"      { Token { symbol = Colon } }
  ";"      { Token { symbol = SemiColon } }
  ","      { Token { symbol = Comma } }
  "{"      { Token { symbol = LBrace } }
  "}"      { Token { symbol = RBrace } }
  "["      { Token { symbol = LBracket } }
  "]"      { Token { symbol = RBracket } }
  "="      { Token { symbol = Equal } } 

  "->"     { Token { symbol = Arrow  } } 
  "--"     { Token { symbol = Dash   } } 


  "graph"    { Token { symbol = Key "graph" } }
  "strict"   { Token { symbol = Key "strict" } }
  "digraph"  { Token { symbol = Key "digraph" } }
  "subgraph" { Token { symbol = Key "subgraph" } }
  "node"     { Token { symbol = Key "node" } }
  "edge"     { Token { symbol = Key "edge" } }

  float      { Token { symbol = Float _ }}
  integer    { Token { symbol = Int _ } }
  string     { Token { symbol = String _ } }
  

%%  

Graph :: { Graph }
  : StrictFlag GraphType MaybeID "{" Statements "}" { Graph $1 $2 $3 $5 }

StrictFlag :: { Bool }
  : "strict"  { True }
  |           { False }

GraphType :: { GraphType }
  : "digraph" { Directed }
  | "graph"   { UnDirected }

MaybeID :: { Maybe ID }
  : ID     { Just $1 }
  |        { Nothing }

ID :: { ID }
  : float   { FloatID  $ fromFloat $1 }
  | integer { IntID    $ fromInt   $1 }
  | string  { StringID $ fromString $1 }
  
Statements :: { [Statement] }
  : Statement StatementSep  Statements { $1 : $3 }
  |                                    { [] }

StatementSep
  : ";" {}
  |     {}

Statement :: { Statement }
  : NodeStatement { $1 }
  | EdgeStatement { $1 }
  | AttrStatement { $1 }
  | ID "=" ID     { AssignmentStatement $1 $3 }
  | Subgraph      { SubgraphStatement $1 }

NodeStatement :: { Statement }
  : NodeID Attributes { NodeStatement $1 $2 }

EdgeStatement :: { Statement }
  : Source Destinations Attributes { EdgeStatement $1 $2 $3}

Source :: { NodeSub }
  : NodeID       { OfNode     $1 }
  | Subgraph     { OfSubgraph $1 }

Destinations :: { [Destination] }
  : EdgeTo Destinations  { $1 : $2 }
  | EdgeTo               { [$1] }

EdgeTo :: { Destination }
  : "->"  Source { DirectedEdge $2 }
  | "--"  Source { UnDirectedEdge $2 }

Attributes :: { [ Attribute ] }
  : "[" AList "]" Attributes { $2 ++ $4 }
  | "[" AList "]"            { $2 }

AList :: { [ Attribute ] }
  : ID "=" ID AListSep AList { Attr $1 $3 : $5 }
  | ID "=" ID AListSep       { [Attr $1 $3] }

AListSep
  : ";" {}
  | "," {}
  |     {}

Subgraph :: { Subgraph }
  : SubgraphHead "{" Statements "}" { Subgraph $1 $3 }

SubgraphHead :: { Maybe ID }
  : "subgraph" MaybeID { $2 }
  |                    { Nothing }

NodeID :: { NodeID }
  : ID      { NodeID $1 Nothing }
  | ID Port { NodeID $1 (Just $2) }

Port :: { Port }
  : ":" ID MaybeCompass { maybe (testCompass $2) (\c -> PortI $2 (Just c)) $3 }


MaybeCompass
  : ":" Compass         { Just $2 }
  |                     { Nothing }

Compass :: { Compass }
  : string { toCompass $ fromString $1 }


AttrStatement :: { Statement }
  : "graph" Attributes { AttrStatement ASGraph $2 }
  | "node"  Attributes { AttrStatement ASNode  $2 }
  | "edge"  Attributes { AttrStatement ASEdge  $2 }

{

fromString :: Token -> String
fromString (Token { symbol = String s } ) = s

fromInt :: Token -> Integer
fromInt (Token { symbol = Int s } ) = s

fromFloat :: Token -> Double
fromFloat (Token { symbol = Float s } ) = s 
  
toCompass :: String -> Compass
toCompass "n"  = N
toCompass "ne" = NE
toCompass "e"  = E
toCompass "se" = SE 
toCompass "s"  = S
toCompass "sw" = SW
toCompass "w"  = W
toCompass "nw" = NW
toCompass "c"  = C 
toCompass _    = O

testCompass :: ID -> Port
testCompass (StringID s) =
  case toCompass s of
    O -> PortI (StringID s) Nothing
    c -> PortC c
testCompass x = PortI x Nothing 

lexer :: (Token -> Alex a) -> Alex a
lexer k = do
  t <- alexMonadScan
  k t 
  

parseDot :: String -> Either String Graph
parseDot s = runAlex s pGraph 
  
parseError :: Token -> Alex a
parseError t = alexError msg
  where
    n = line   t
    c = column t
    msg = "Parse Error at: " ++ "line: " ++ show n ++ " column: " ++ show c

}

