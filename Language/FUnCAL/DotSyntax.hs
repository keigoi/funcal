module Language.FUnCAL.DotSyntax where

import Control.DeepSeq 

data Graph = Graph Bool GraphType (Maybe ID) [Statement]

data GraphType = Directed | UnDirected

data ID        = StringID String
               | IntID    Integer
               | FloatID  Double
               | HTMLID   String
               deriving (Eq, Ord, Show)


instance NFData ID where
  rnf (StringID s) = rnf s
  rnf (IntID i)    = rnf i
  rnf (FloatID f)  = rnf f
  rnf (HTMLID h)   = rnf h

data Statement
  = NodeStatement NodeID  [Attribute]
  | EdgeStatement NodeSub [Destination] [Attribute]
  | AttrStatement ASType  [Attribute]
  | AssignmentStatement ID ID
  | SubgraphStatement   Subgraph 

data ASType = ASGraph | ASEdge | ASNode    

data Destination = DirectedEdge   NodeSub
                 | UnDirectedEdge NodeSub

data NodeSub = OfNode     NodeID
             | OfSubgraph Subgraph     

data Subgraph = Subgraph (Maybe ID) [Statement]

data Attribute = Attr ID ID

data NodeID = NodeID ID (Maybe Port) 
data Port = PortI ID (Maybe Compass)
          | PortC Compass

data Compass = N | NE | E | SE | S | SW | W | NW | C | O             

                 
               
