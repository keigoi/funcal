{-# LANGUAGE MultiParamTypeClasses, FlexibleInstances,
             UndecidableInstances, BangPatterns, TypeFamilies, RankNTypes, 
             NoMonomorphismRestriction, IncoherentInstances, CPP  #-}
{-# LANGUAGE TypeApplications, ScopedTypeVariables #-}


module Language.FUnCAL.Internal where 

import Data.IORef 
-- import Data.Unique 

import qualified Data.Set as S

import qualified Data.IntMap as IM
import qualified Data.IntSet as IS  

import Control.Monad 
import Control.Monad.Identity 
import Control.Monad.Writer 

import Data.Coerce

import System.IO.Unsafe 

#ifdef __DEBUG__
import Debug.Trace 
#endif

import Control.Monad.ST (ST, runST)
import Data.List (foldl') 
import Data.STRef (STRef, modifySTRef', newSTRef, readSTRef, writeSTRef)

import Language.FUnCAL.Graph
import Language.FUnCAL.Shaped as Sh

import Language.FUnCAL.Internal.Memo

import Data.Vector (Vector)
import qualified Data.Vector as V 



debug :: String -> a -> a 
#ifdef __DEBUG__
debug = trace
#else
debug _ = id
#endif 

-- | The datatype for FUnCAL expressions. Currently, the generation check is not
--   implemented either statically or dynamically. 
data G m a = GEmp 
           | GCons  a !(G m a)
           | GUnion !(G m a)   !(G m a)
           | GRef   !(Loc m (Thunk m (G m a)))

-- | For easy debugging. With this instance, one can 'print' the
--   evaluation results of FUnCAL expressions in "ghci".
--   
--   Note that this implementation uses 'unsafePerformIO', and this is
--   why one can receive different printing results for the same
--   expression.
instance Show a => Show (G IO a) where
  show x = show $ unsafePerformIO $ asTerm x
  showList x s = unlines (map show x) ++ s 

-- | Closed version of @m (G m a)@.
--   Currently, this datatype is used just to 'show' the evaluation result of
--   FUnCAL expressions. 
newtype ClosedG a = ClosedG (forall m. (Memoizing m, MonadFix m) => m (G m a))

-- | Witnessing closedness. 
close :: (forall m. (Memoizing m, MonadFix m) => m (G m a)) -> ClosedG a
close = ClosedG

-- | Forgetting closedness witness. 
open :: (Memoizing m, MonadFix m) => ClosedG a -> m (G m a)
open (ClosedG g) = g 

-- | To 'show' graphs, please 'show' them via 'ClosedG'. 
instance Show a => Show (ClosedG a) where
  show (ClosedG g) = show $ runMemoST (asTerm =<< g)
  showList x s    = unlines (map show x) ++ s 


-- | To construct the empty graph.
--
-- >>> close end
-- Root:
--   *
-- Heap:
{-# INLINABLE[1] end #-}
end :: Memoizing m => m (G m a)
end = return GEmp 

{-# INLINE[1] consG #-}
{-# SPECIALIZE consG :: MonadIO m => a -> G (MemoIO m) a -> MemoIO m (G (MemoIO m) a) #-}
{-# SPECIALIZE consG :: a -> G IO a -> IO (G IO a) #-}
{-# SPECIALIZE consG :: a -> G (MemoST s) a -> MemoST s (G (MemoST s) a) #-}
-- | Edge extension
--
-- >>> close $ consG "A" =<< end
-- Root:
--   @1
-- Heap:
--   @1: 	"A" : *
consG :: Memoizing m => a -> G m a -> m (G m a)
consG a x =
  GRef <$> newLoc (Evaluated $ GCons a x)

{-# INLINABLE[1] consGUS #-}
consGUS :: Memoizing m => a -> G m a -> m (G m a)
consGUS a x =
  return $ GCons a x


{-# RULES
-- "consG/consG"     forall a b x. consG a x >>= consG   b = consGUS a x >>= consG b
-- "consGUS/consG"    forall a b x. consG a x >>= consGUS b = consGUS a x >>= consGUS b
-- "consG/unionG"    forall a x y. unionG x y >>= consG a  = unionGUS x y >>= consG a
-- "consUS/unionG"   forall a x y. unionG x y >>= consGUS a = unionGUS x y >>= consGUS a
"mconcat1/consG" forall a x. mconcat [consG a x] = consG a x
"mconcat2/consG" forall a b x y. mconcat [consG a x, consG b y]
                                 = mconcat [consGUS a x, consGUS b y]
"mconcat3/consG" forall a b c x y z. mconcat [consG a x, consG b y, consG c z]
                                 = mconcat [consGUS a x, consGUS b y, consGUS c z]
"mappend/consG-left"  forall a x y. mappend (consG a x) y = 
                                      mappend (consGUS a x) y
"mappend/consG-right" forall b x y. mappend x (consG b y) =
                                      mappend x (consGUS b y)
"consG/end"           forall a. end >>= consG a = consG a GEmp 
  #-}


            -- do { r <- newRef x 
  --              ; u <- newUniq 
  --              ; return $ GCons u a r }

-- | A convenient operator.
--
-- >>> close $ "A" <:> end
-- Root:
--  @1
-- Heap:
--  @1: 	"A" : *
--
-- === Property 
-- prop> a <:> m = m >>= consG a 
{-# INLINABLE (<:>) #-}
a <:> m = m >>= consG a 

infixr 5 <:> 

instance Memoizing m => Monoid (m (G m a)) where
  mempty  = return GEmp
  {-# INLINABLE mempty #-}
  mappend mx my = do
    x <- mx
    y <- my
    unionG x y
  {-# INLINABLE mappend #-}

  mconcat = mconcatUS
  {-# INLINE[1] mconcat #-}

-- | Edge-set union
--
-- >>> close $  ("A" <:> end) >>= \x -> ("B" <:> end) >>= \y -> unionG x y
-- Root:
--  @3
-- Heap:
--  @3: 	@1 U @2
--  @2: 	"B" : *
--  @1: 	"A" : *
--
-- Notice that @end >>= unionG x@ and @end >>= flip unionG x@
-- equivalent to @x@ in FUnCAL, while their internal representations
-- (obtained by 'show') could differ.
--
-- Using 'Monoid' method would be more convenient, as:
--
-- >>> close $ ("A" <:> end) <> ("B" <:> end)
-- Root:
--  @3
-- Heap:
--  @3: 	@1 U @2
--  @2: 	"B" : *
--  @1: 	"A" : *

{-# INLINABLE[1] unionG #-}
unionG :: Memoizing m => G m a -> G m a -> m (G m a)
unionG x y =
  GRef <$> newLoc (Evaluated $ gunion x y)

{-# INLINABLE gunion #-}
gunion GEmp y = y
gunion x GEmp = x
gunion x y    = GUnion x y 

unionGUS :: Memoizing m => G m a -> G m a -> m (G m a)
unionGUS x y = return $ gunion x y 

mconcatUS :: Memoizing m => [m (G m a)] -> m (G m a)
mconcatUS []  = end
mconcatUS [x] = x
mconcatUS xs  = sequence xs >>= unionsG 

  

{-# RULES
"unionG/unionG1" forall x y z. unionG x y >>= (`unionG` z) = unionGUS x y >>= (`unionG` z)
"unionG/unionG2" forall x y z. unionG x y >>= unionG z = unionGUS x y >>= unionG z
"consG/unionG1"  forall a x z. consG a x >>= (`unionG` z) = consGUS a x >>= (`unionG` z)
"consG/unionG2"  forall a x z. consG a x >>= unionG z = consGUS a x >>= unionG z
  #-}


-- | List-version of 'unionG'
{-# INLINABLE[1] unionsG #-}
{-# SPECIALIZE unionsG :: MonadIO m => [ G (MemoIO m) a ] -> MemoIO m (G (MemoIO m) a) #-}
{-# SPECIALIZE unionsG :: [ G IO a ] -> IO (G IO a) #-}
{-# SPECIALIZE unionsG :: [ G (MemoST s) a ] -> MemoST s (G (MemoST s) a) #-}
unionsG :: Memoizing m => [ G m a ] -> m (G m a)
unionsG []  = return GEmp 
unionsG [x] = return x
unionsG xs  =
  let graph = foldl' gunion GEmp xs
  in fmap GRef (newLoc $! (Evaluated $! graph))


{-# INLINE initValue #-}
initValue :: Memoizing m => m (G m a)
initValue = GRef <$> newLoc (Evaluated GEmp)

{-# INLINE assignValue #-}
assignValue :: Memoizing m => G m a -> m (G m a) -> m ()
assignValue (GRef loc) g = writeLoc loc (Suspended g)


-- | Mutual recursive definition of graphs to construct cyclic graphs
--
-- >>> close $ head <$> gfixG 2 (\[x,y] -> sequence ["A" <:> return y, "B" <:> return x])
-- Root:
--  @1
-- Heap:
--  @1: 	"A" : @2
--  @2: 	"B" : @1
--
-- Interestingly, the following expression terminates!
-- 
-- >>> close $ head <$> gfixG 2 (\[x,y] -> sequence [return y, return x])
-- Root:
--  @1
-- Heap:
--  @1: 	*
{-# SPECIALIZE
    gfixG :: MonadIO m =>
             Int -> ([G (MemoIO m) a] -> MemoIO m [G (MemoIO m) a])
             -> MemoIO m [G (MemoIO m) a] #-}
{-# SPECIALIZE
    gfixG :: MonadIO m =>
             Int -> (Vector (G (MemoIO m) a) -> MemoIO m (Vector (G (MemoIO m) a)))
             -> MemoIO m (Vector (G (MemoIO m) a)) #-}
{-# SPECIALIZE
    gfixG :: Int -> ([G IO a] -> IO [G IO a]) -> IO [G IO a] #-}
{-# SPECIALIZE
    gfixG :: Int -> (Vector (G IO a) -> IO (Vector (G IO a)))
             -> IO (Vector (G IO a)) #-}
{-# SPECIALIZE
    gfixG :: Int -> ([G (MemoST s) a] -> MemoST s [G (MemoST s) a])
             -> MemoST s [G (MemoST s) a] #-}
{-# SPECIALIZE
    gfixG :: Int -> (Vector (G (MemoST s) a) -> MemoST s (Vector (G (MemoST s) a)))
             -> MemoST s (Vector (G (MemoST s) a)) #-}
gfixG :: (Shaped t, Memoizing m) =>
         Shape t -> (t (G m a) -> m (t (G m a)))
         -> m (t (G m a))
gfixG !n f = do
  xs <- generateM n (const initValue)
  let m = f xs
  ref <- newLoc $ Suspended m 
  imapM_ (\i r -> assignValue r $ do
             x <- resolveLoc ref
             return $ index x i) xs 
  return xs

-- | A specialized version of 'gfixG'.
--   The expression @fixG f@ is equivalence to @fmap head . gfixG 1 (\[x] -> sequence [f x])@,
--   but is more efficient. 
{-# SPECIALIZE fixG :: (G IO a -> IO (G IO a)) -> IO (G IO a) #-}
{-# SPECIALIZE fixG :: (G (MemoST s) a -> MemoST s (G (MemoST s) a)) -> MemoST s (G (MemoST s) a) #-}
{-# INLINE[1] fixG #-}
fixG :: Memoizing m => (G m a -> m (G m a)) -> m (G m a)
fixG f = do
  x <- initValue
  assignValue x (f x)
  return x 


{-# RULES 
"fixG-unionG2" forall e. fixG (`unionG` e) = return e
"fixG-unionG1" forall e. fixG (unionG e) = return e 
  #-}


-- | call-by-need variable dereference 
{-# SPECIALIZE readLocUpdate ::
                  Loc IO (Thunk IO (G IO a)) -> IO (G IO a) #-}
{-# SPECIALIZE readLocUpdate ::
                  Loc (MemoST s) (Thunk (MemoST s) (G (MemoST s) a)) -> MemoST s (G (MemoST s) a) #-}
readLocUpdate :: Memoizing m => Loc m (Thunk m (G m a)) -> m (G m a)
readLocUpdate loc = go loc IS.empty
  where
    go loc proced
      | IS.member (locID loc) proced = do
        writeLoc loc (Evaluated GEmp)
        return GEmp
      | otherwise = do 
        t <- readLoc loc
        (v, b)  <- case t of
          Suspended m -> do 
            v <- m
            return (v, True)
          Evaluated v -> 
            return (v, False) 
        case v of
          GRef loc' -> do 
            v <- go loc' $! IS.insert (locID loc) proced
            writeLoc loc (Evaluated v)
            return v
          gg -> do 
            when b $ writeLoc loc (Evaluated gg)
            return gg


-- | Structural recursion. 
{-# SPECIALIZE
  gparaG :: MonadIO m =>
            Int -> (a -> G (MemoIO m) a -> [G (MemoIO m) a] -> MemoIO m [G (MemoIO m) a]) ->
            G (MemoIO m) a -> MemoIO m [G (MemoIO m) a]
 #-}
{-# SPECIALIZE
  gparaG :: MonadIO m =>
            Int -> (a -> G (MemoIO m) a -> Vector (G (MemoIO m) a) -> MemoIO m (Vector (G (MemoIO m) a))) ->
            G (MemoIO m) a -> MemoIO m (Vector (G (MemoIO m) a))
 #-}
{-# SPECIALIZE
  gparaG :: Int -> (a -> G IO a -> [G IO a] -> IO [G IO a]) ->
            G IO a -> IO [G IO a]
 #-}
{-# SPECIALIZE
  gparaG :: Int -> (a -> G IO a -> Vector (G IO a) -> IO (Vector (G IO a))) ->
            G IO a -> IO (Vector (G IO a))
 #-}
{-# SPECIALIZE
  gparaG :: Int -> (a -> G (MemoST s) a -> Vector (G (MemoST s) a) -> MemoST s (Vector (G (MemoST s) a))) ->
            G (MemoST s) a -> MemoST s (Vector (G (MemoST s) a))
 #-}
{-# INLINABLE[1] gparaG #-}
gparaG :: (Memoizing m, Shaped t) =>
          Shape t -> (a -> G m a -> t (G m a) -> m (t (G m a))) ->
          G m a -> m (t (G m a))
gparaG !n f = \arg -> do
  !memo <- newRef IM.empty
  go memo arg
    where      
      go memo v =
        case v of
          GEmp      ->
            generateM n (const end) 
          GCons a x ->
            go memo x >>= f a x
          GUnion x y -> do 
            x' <- go memo x 
            y' <- go memo y 
            Sh.zipWithM unionG x' y'
          GRef loc -> goLoc memo loc 

      goLoc memo loc = do
        let !key = locID loc 
        tb <- readRef memo
        case IM.lookup key tb of
          Just ws ->
            debug ("Lookup succeeds: " ++ show key ++ " -> " ++ showR ws) $
            return ws
          Nothing -> do 
            ws <- generateM n (const initValue) 
            writeRef memo (IM.insert key ws tb)
            debug ("Inserted entry:  " ++ show key ++ " -> " ++ showR ws) $ return ()
            let resultsM = readLocUpdate loc >>= go memo
            ref <- newLoc (Suspended resultsM)
            imapM_ (\i r ->
                       assignValue r $ do 
                         rs <- resolveLoc ref -- resultsM
                         return $ index rs i) ws 
            -- zipWithM_ assignValue ws $
            --   map (\i -> do { rs <- resultsM; return $ rs !! i }) [0..(n-1)]
            return ws

        where
          showR ws = show (toList $ fmap unGRef ws)        
            where
              unGRef (GRef loc) = loc 
            

-- | A specialized version of 'gparaG 1'.
paraG :: Memoizing m => 
         (a -> G m a -> G m a -> m (G m a))
             -> G m a -> m (G m a)         
-- paraG f arg = head <$> gparaG 1 (\l g r -> (:[]) <$> f l g (head r)) arg 
paraG f arg = coerce <$> gparaG 1 (\l g  r -> I <$> f l g (coerce r)) arg 
{-# INLINABLE paraG #-}


newtype I a = I { runI :: a }

instance Functor I where
  fmap f = coerce . f . coerce

instance Foldable I where
  foldMap f (I a) = f a 

instance Traversable I where
  traverse f (I a) = fmap I (f a)

instance Shaped I where
  type Shape I = Int
  type Index I = Int

  indices _ = [0]
  size _    = 1
  shape _   = 1
  
  generate _ f = I (f 0)
  generateM _ f = fmap I (f 0)
  
  imap f (I x) = I (f 0 x)
  imapM_ f (I x) = void (f 0 x)
  
  zipWith f (I a) (I b) = I (f a b)

  index (I a) _ = a


{- | An experimental interface for "recursive do".

The following examples work as expected (the former
returns a graph with a cycle "-1-> -2-> -1-> -2-> ..."
and the latter results in the empty graph).


> test_graph = do
>   rec a <- graph $ 1 <:> return b
>       b <- graph $ 2 <:> return a
>  return a
> 
> test_graph2 = do
>   rec a <- graph $ return b
>       b <- graph $ return a
>   return a

Be careful! If you forget 'graph', both example run forever. 
-}
graph :: Memoizing m => m (G m a) -> m (G m a)
graph m = do
  loc <- newLoc (Suspended m)
  return $ GRef loc

{- |
An experimental interface for "recursive do". 

The function is nothing but @graph . return@. We assume that this
function is used to replace 'return' in recursive definition.

> test_graph' = do
>   rec a <- 1 <:> connect b
>       b <- 2 <:> connect a
>   return a 
> 
> test_graph2' = do
>   rec a <- connect b
>       b <- connect a
>   return a
-} 

connect :: Memoizing m => G m a -> m (G m a)
connect g = graph (return g)


-- test_graph3 = do
--   rec a <- graph $ const (1 <:> return a) b 
--       b <- graph $ (3 <:> return b) <>
--                    (paraG (\a _ r -> a <:> 2 <:> return r) =<< return a)
--   return b

-- test_graph3' = do
--   rec a <- const (1 <:> connect a) b 
--       b <- (3 <:> connect b) <>
--            (paraG (\a _ r -> a <:> 2 <:> return r) =<< connect a)
--   return b

{- |
An experimental interface for "recursive do".

The following example illustrates how we can use 'srec'. 


> test_srec = do
>   rec a <- graph $ ("A" <:> "B" <:> "C" <:> return a) <> return b
>       b <- graph $ "C" <:> return b 
>   rec f <- srec $ \a x ->
>         if a == "A" then      "D" <:> f x
>         else if a == "C" then f x 
>         else                  a  <:> return x
>   f a 

Caution! It is important to restrict the argument of structural
recursion (@f@ above) must be a variable bound in @srec $ \a x ->
...@.  Otherwise, it can be the case that the defined structural
recursion runs forever.
-}


srec :: forall m a b. Memoizing m => (a -> G m a -> m (G m b)) -> m (G m a -> m (G m b))
srec f = do
  !memo <- newRef IM.empty
  return (f' memo)
    where
      f' :: MemoRef m (IM.IntMap (G m b)) -> G m a -> m (G m b)
      f' memo v = 
        case v of
          GEmp       -> return GEmp
          GCons a x  -> f a x
          GUnion x y -> 
            f' memo x <> f' memo y
          GRef loc -> do 
            let key = locID loc 
            tb <- readRef memo
            case IM.lookup key tb of
              Just v  -> return v
              Nothing -> do
                v <- initValue
                writeRef memo $ IM.insert key v tb
                let resultM = readLocUpdate loc >>= f' memo
                assignValue v resultM 
                return v
          


  
-- | Checks if a graph has at least one edge or not.
{-# SPECIALIZE isEmptyG :: G IO a -> IO Bool #-}
{-# SPECIALIZE isEmptyG :: G (MemoST s) a -> MemoST s Bool #-}
isEmptyG :: Memoizing m => G m a -> m Bool
isEmptyG v = do
  memo <- newRef IS.empty
  go memo v
    where
      go memo v = case v of
        GEmp       -> return True
        GCons _ _  -> return False
        GUnion x y ->
          liftM2 (&&) (go memo x) (go memo y)
        GRef loc -> goLoc memo loc

      goLoc memo loc = do {
        ; let key = locID loc
        ; tb <- readRef memo
        ; if IS.member key tb then
            return True
          else do
            writeRef memo $! IS.insert key tb
            readLocUpdate loc >>= go memo }

-- -- | A version of `toGraph` to obtain epsilon-free graphs 
-- {-# SPECIALIZE toGraph' :: Ord a => G Memo a -> Memo (Graph a) #-}
-- {-# SPECIALIZE toGraph' :: Ord a => G IO a -> IO (Graph a) #-}
-- {-# SPECIALIZE toGraph' :: Ord a => G (MemoST s) a -> MemoST s (Graph a) #-}
-- toGraph' :: (Memoizing m, Ord a) => G m a -> m (Graph a)
-- toGraph' = toGraph 


-- | Evaluates a FUnCAL expression to obtain a graph.
{-# SPECIALIZE toGraph :: Ord a => G IO a -> IO (Graph a) #-}
{-# SPECIALIZE toGraph :: Ord a => G (MemoST s) a -> MemoST s (Graph a) #-}
toGraph :: (Memoizing m, Ord a) => G m a -> m (Graph a)
toGraph !v = removeDup <$> do
  memo <- newRef IM.empty
  counter <- newRef 0
  n <- newNode counter 
  (eMap, r, b) <- go n v counter memo (IM.fromList [(0,[]), (n,[])])
  return $ Graph r (if b then IM.insertWith (++) n [] eMap else eMap)
    where     
      newNode counter = do
        i <- readRef counter
        let j = i+1
        writeRef counter $! j
        return j

      go n v counter memo eMap = 
        case v of
          GEmp ->
            return (eMap, 0, False)
          GCons a x -> do
            n' <- newNode counter
            (eMap', r, _) <- go n' x counter memo eMap
            let !eMap'' = IM.insertWith (++) n [LabTo a r] eMap'
            return (eMap'', n, True)
          GUnion x y -> do
            (eMap1, r1, b1) <- go n x counter memo eMap
            (eMap2, r2, b2) <- go n y counter memo eMap1
            let !b = b1 || b2 
            return (eMap2, n, b)
            -- n1 <- newNode counter 
            -- n2 <- newNode counter 
            -- (eMap1,r1,_) <- go n1 x counter memo eMap 
            -- (eMap2,r2,_) <- go n2 y counter memo eMap1
            -- return (IM.insertWith (++) n [EpsTo r1, EpsTo r2] eMap2, n, True)
          GRef loc -> goLoc n loc counter memo eMap

      goLoc n loc counter memo eMap = do
        let key = locID loc 
        tb <- readRef memo
        case IM.lookup key tb of
          Just n  -> return (eMap, n, True)
          Nothing -> do 
            writeRef memo $! IM.insert key n tb
            v <- readLocUpdate loc 
            (eMap, r, b) <- go n v counter memo eMap
            let eMap' = IM.insertWith (++) n [] eMap 
            if b then return (eMap, r, b) else return (eMap', r, True)
            

data TermRepl a = TEmp
                | TCons a (TermRepl a)
                | TUnion  (TermRepl a) (TermRepl a)
                | TLoc    LocInt 

type HeapRepl a = [(LocInt, TermRepl a)]                  
                  
newtype LocInt = LocInt Int 

instance Show LocInt where
  show (LocInt 0) = "NULL"
  show (LocInt x) = "@" ++ show x 

-- | The datatype 'Repl' is provided just for 'show'. By 'Repl', we
--   can 'show' something similar to the internal representation of an
--   evaluation result.
data Repl a = Repl (TermRepl a) (HeapRepl a)

instance Show a => Show (Repl a) where
  show (Repl i hs) =
    "Root:\n"++
    "  " ++ show i ++ "\n" ++ 
    "Heap:\n"++
    concat [ "  " ++ show i ++ ": \t" ++ show t ++ "\n" |  (i,t) <- hs ]

instance Show a => Show (TermRepl a) where
  show = go False
    where
      parensIf b s = if b then "(" ++ s ++ ")" else s 
      
      go b TEmp = "*"
      go b (TCons a x)  = parensIf b $
                          show a ++ " : " ++ go False x
      go b (TUnion x y) = parensIf b $
                          go True x ++ " U " ++ go True y
      go b (TLoc loc)   = show loc 
                          

-- | Converts 'G' to 'Repl'
asTerm ::  Memoizing m => G m a -> m (Repl a) 
asTerm v = do
  memo    <- newRef IS.empty 
  (es,r)  <- go v memo []
  return $ Repl r es 
    where
      goLoc loc memo aes = do {
        ; let key = locID loc
        ; tb <- readRef memo
        ; if IS.member key tb then 
            return (aes, TLoc (LocInt key))
          else do 
            writeRef memo $! IS.insert key tb
            v <- readLocUpdate loc
            (es, r) <- go v memo aes
            return ((LocInt key, r):es, TLoc (LocInt key))
        }
      
      go v memo aes = 
        case v of 
          GEmp      -> return (aes, TEmp) 
          GCons a x -> do 
            (es,r) <- go x memo aes 
            return (es, TCons a r)
          GUnion x y -> do 
            (es1,r1) <- go x memo aes 
            (es2,r2) <- go y memo es1 
            return (es2, TUnion r1 r2)
          GRef loc -> goLoc loc memo aes 



-- | Inverse of 'toGraph' (up to bisimilarity)
{-# SPECIALIZE fromGraph :: Graph a -> IO (G IO a) #-}
{-# SPECIALIZE fromGraph :: Graph a -> MemoST s (G (MemoST s) a) #-}
fromGraph :: Memoizing m => Graph a -> m (G m a) 
fromGraph graph' = do 
  memo   <- newRef IM.empty 
  go root memo 
    where
      Graph root eMap = eliminateEps graph' 

      go n memo = do
        tb <- readRef memo
        case IM.lookup n tb of
          Just v -> return v
          Nothing ->
            case IM.lookup n eMap of
              Nothing -> return GEmp 
              Just [] -> return GEmp 
              Just us -> do
                loc <- newLoc $! Evaluated GEmp
                writeRef memo $! IM.insert n (GRef loc) tb
                writeLoc loc $ Suspended $ do 
                  vs <- mapM (\(LabTo a u) -> do
                                 r <- go u memo 
                                 return $ GCons a r) us
                  unionsG vs
                return $ GRef loc 
                

{-
test1 = fmap head $ gfixG 1 (\[x] -> sequence [consG "A" x])


test2 = fmap head $ gfixG 3 $ 
             (\[x,y,z] -> sequence [ mconcat ["A" <:> (consG "B" z),
                                              "A" <:> (consG "A" z),
                                              "C" <:> return y],
                                     "C" <:> return y, 
                                     "A" <:> "B" <:> empG ])


test3 = fmap head $ gfixG 2 $ 
             (\[x,y] -> sequence [ mconcat [consG  "C" y],
                                   consG "C" y])


test4 = fmap head $ gfixG 3 (\[x,y,z] ->
              sequence [ mconcat [ "A" <:> (return y),
                                   "B" <:> (return y) ],
                         mconcat [ "A" <:> (return z), 
                                   "B" <:> (return z) ],
                         mconcat [ "A" <:> empG,
                                   "B" <:> empG ] ])

a2d_xc = gparaG 1 (\a _ [x] -> 
                       if a == "A" then 
                           sequence ["D" <:> (return x)]
                       else if a == "C" then 
                              return [x]
                            else
                              sequence [consG a x])
-}                             

    
