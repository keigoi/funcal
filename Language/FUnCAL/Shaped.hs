{-# LANGUAGE TypeFamilies, BangPatterns, AllowAmbiguousTypes, TypeApplications, ScopedTypeVariables #-}

{- |

Constrainer-like datatypes. Actually, they must be isomorphic to
'Traversable's, but we allow flexiblity to choose representation of
shapes (e.g., to use 'Int' for shapes of lists).

-}
module Language.FUnCAL.Shaped where

import qualified Data.IntMap as IM
import Data.Maybe (fromJust)
import Data.Vector (Vector)
import qualified Data.Vector as V
import qualified Prelude
import Prelude hiding (zipWith)

-- | Container-like datatypes. 
class (Functor t, Traversable t) => Shaped (t :: * -> *) where
  type Shape t :: *
  type Index t :: *

  -- | To make a constructor by giving a shape and mapping from indices to contents. 
  generate  :: Shape t -> (Index t -> a) -> t a

  -- | The shape of a container. 
  shape   :: t a -> Shape t

  -- | Set of valid indices in a container
  indices :: t a -> [Index t]

  -- | The number of valid indices. 
  size :: t a -> Int
  size = length . indices 

  -- | The number of valid indices can be determined only by its shape. 
  sizeSh :: Shape t -> Int
  sizeSh (sh :: Shape t) = size (generate @t (sh :: Shape t) (const ()))

  -- | A monadic version of 'generate'
  {-# INLINABLE generateM #-}
  generateM :: Monad m => Shape t -> (Index t -> m a) -> m (t a)
  generateM n f = sequence (generate n f)

  -- | 'map' with index. 
  imap :: (Index t -> a -> b) -> t a -> t b
  imap f t = zipWith f (generate (shape t) id) t

  -- | A monadic version of 'imap'.
  {-# INLINABLE imapM #-}
  imapM     :: Monad m => (Index t -> a -> m b) -> t a -> m (t b)
  imapM f xs = sequence $ imap f xs

  -- | A similar to 'imapM', but 'imapM_' discards the computation result. 
  {-# INLINABLE imapM_ #-}
  imapM_   :: Monad m => (Index t -> a -> m b) -> t a -> m ()
  imapM_ f xs = sequence_ $ imap f xs

  -- | Extracts a content specified by a given index. 
  index :: t a -> Index t -> a

  -- | A generic version of 'zipWith'. 
  zipWith :: (a -> b -> c) -> t a -> t b -> t c
  zipWith f xs ys = imap (\k a -> f a (index ys k)) xs 

  -- | A generic version of 'zip'. 
  zip :: t a -> t b -> t (a,b)
  zip = zipWith (,)
  {-# INLINABLE zip #-}
  
  -- | A generic version of 'zipWithM'. 
  zipWithM :: Monad m => (a -> b -> m c) -> t a -> t b -> m (t c)
  zipWithM f xs ys = sequence $ zipWith f xs ys
  {-# INLINABLE zipWithM #-}


  -- | Extracts all the contents. 
  toList :: t a -> [a]
  toList t = [ index t k | k <- indices t  ]

  -- | 'show'-like function. 
  showShaped :: Show a => t a -> String
  showShaped t = show (toList t)

  {-# MINIMAL generate, shape, indices, index, (zipWith | imap) #-}

instance Shaped V.Vector where
  type Shape Vector = Int
  type Index Vector = Int

  shape = V.length
  indices t = [0 .. V.length t - 1]
  toList = V.toList

  size = V.length
  sizeSh = id

  generate = V.generate
  {-# INLINABLE generate #-}
  
  imap     = V.imap
  {-# INLINABLE imap #-}

  index = V.unsafeIndex
  {-# INLINABLE index #-}

  zipWith = V.zipWith 

instance Shaped [] where
  type Shape [] = Int
  type Index [] = Int

  shape = length
  indices t = [0..length t - 1]

  size = length 
  sizeSh = id 

  toList = id 

  generate n f = map f [0..(n-1)]
  {-# INLINABLE generate #-}
  
  imap f = Prelude.zipWith f [0..] 
  {-# INLINABLE imap #-}

  {-# SPECIALIZE imapM_ :: (Int -> a -> IO b) -> [a] -> IO () #-}
  imapM_ f xs = go 0 xs
    where
      go !i []   = return ()
      go i (v:x) = do
        f i v
        go (i+1) x 
    
  
  index = (!!)
  {-# INLINABLE index #-}

  zipWith = Prelude.zipWith 

instance Shaped IM.IntMap where
  type Shape IM.IntMap = [Int]
  type Index IM.IntMap = Int

  shape   = IM.keys
  indices = IM.keys

  toList  = IM.elems

  size = IM.size 
  sizeSh = length 

  generate ks f = IM.fromList (map (\k -> (k,f k)) ks)
  {-# INLINABLE generate #-}

  imap = IM.mapWithKey
  {-# INLINABLE imap #-}

  index m k = fromJust $ IM.lookup k m 
  {-# INLINABLE index #-}

  zipWith f xs ys = IM.mapWithKey (\k a -> f a (index ys k)) xs


