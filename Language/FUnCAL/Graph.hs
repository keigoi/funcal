{-# LANGUAGE DeriveFunctor, PatternGuards #-}

{- | Rooted, edge-labeled, graphs with epsilon edges. -}
module Language.FUnCAL.Graph (
  Edge(..), Graph(..), uncalString, dotString, UnCALLabel(..), 
  fromDot, fromDotFile, simplify, eliminateEps, reduce, removeDup,
  vertexSize, edgeSize, Dot.ID(..) 

                             ) where 

import Control.DeepSeq 


import Control.Monad (mplus)
import Control.Monad.State (StateT, execStateT, get, put)
import qualified Data.Graph   as HsGraph 

import qualified Data.IntMap as IM
import qualified Data.IntSet as IS 


import Data.List (isPrefixOf, sort)

import qualified Data.Map as M 
import Data.Maybe (fromJust, fromMaybe)
import qualified Data.Set as S 
import Debug.Trace

import qualified Language.FUnCAL.DotParser as Dot
import qualified Language.FUnCAL.DotSyntax as Dot 
import System.IO (readFile)

import Text.PrettyPrint.HughesPJ

-- | Labels in UnCAL. 
data UnCALLabel
  = LConst  !String
  | LString !String
  | LInt    !Int
  | LFloat  !Double 
  | LBool   !Bool
             deriving (Eq,Ord)

instance Show UnCALLabel where
    show (LConst s)    = s
    show (LString s)   = show s
    show (LInt i)      = show i
    show (LFloat f)    = show f 
    show (LBool True)  = "true"
    show (LBool False) = "false"

instance NFData UnCALLabel where
    rnf (LConst s)  = rnf s
    rnf (LString s) = rnf s
    rnf (LInt i)    = rnf i
    rnf (LFloat f)  = rnf f 
    rnf (LBool l)   = rnf l

-- | Edges. 
data Edge a = EpsTo   !Int 
            | LabTo a !Int 
            deriving (Functor, Eq, Ord)


dst (EpsTo d)   = d
dst (LabTo _ d) = d
                     
instance NFData  a => NFData (Edge a) where 
  rnf (EpsTo i)   = () 
  rnf (LabTo a i) = rnf a 

-- | The datatype for graphs. The internal representation is exposed, but 
--   users must be careful not to distinguish bisimilar graphs. 
data Graph a = Graph { root      :: Int,
                       adjMatrix :: IM.IntMap [Edge a] }     
             deriving Functor 

instance Show a => Show (Edge a) where 
    show (EpsTo i)   = "... " ++ show i 
    show (LabTo a i) = show a ++ " " ++ show i 

instance Show a => Show (Graph a) where 
    show (Graph r vec) = render doc
      where
        doc = hsep [text "R:", 
                    nest 4 (text $ show r)] $$
              -- hsep [text "V:",
              --       nest 4 $ sep (punctuate comma $ map (text . show) $ IM.keys vec) ] $$
              hsep [text "E:",
                    nest 4 $ sep (punctuate comma $ map pprEdges $ IM.toList vec )]
        pprEdges (s, es) =
          text (show s) <+> vcat (punctuate comma $ map pprEdge es)
        pprEdge (EpsTo d) = 
          text "--->" <+> text (show d) 
        pprEdge (LabTo a d) = 
          text "--[" <+> text (show a) <+> text "]->" <+> text (show d)
        

instance NFData a => NFData (Graph a) where 
  rnf (Graph r vec) = rnf vec `seq` rnf r 

-- | Generates a graph in UnCAL format.
uncalString :: Graph String -> String 
uncalString (Graph r vec) =
  -- to avoid bug in UnCAL we unfold a root once.
  render $ goTs (fromJust $ lookup r eM) <> text " @ cycle((" $$ go eM <> text "))"
  where    
    eM = IM.toList vec 
    n i = text ("&xx"++show i)
    go []     = empty 
    go [e]    = goEach e 
    go (e:es) = goEach e <> comma $$ go es 
    goEach (s,outs) = n s <> text" := " <> goTs outs
    goTs ts = braces $ hsep $ punctuate comma (map f ts)
      where
        f (EpsTo x)   = n x 
        f (LabTo l x) = text l <+> colon <+> n x 
    
-- | Generates a graph in "dot" format.
dotString :: Graph String -> String 
dotString (Graph r vec) = 
    render $ text "digraph \"g\" {" $$  
             nest 4 (text "node [shape = \"ellipse\"]") $$
             nest 4 root $$ 
             nest 4 nodes $$
             nest 4 edges $$
             text "}"
    where
      root  = text (show $ show r) <> brackets (text" label = \"{&}\\n\\N\" ")
      nodes = 
          vcat [ text (show $ show v) <>
                      brackets (text" label = \"\\N\" ")
               | v <- IM.keys vec,  v /= r]
      edges = 
          vcat [ text (show $ show v) <> text" -> " <>
                   text (show $ show u)  <+>
                      brackets (text" label = " <> text (show l) <> text " ")
               | (v,es) <- IM.toList vec, LabTo l u <- es ]


data D2GState = D2GState !(M.Map Dot.ID [(Maybe UnCALLabel, Dot.ID)]) !(Maybe Dot.ID) !(Maybe Dot.ID)

-- | Similar to 'fromDot', but this function reads a graph from a file. 
fromDotFile :: String -> IO (Graph UnCALLabel)
fromDotFile filepath =
    either fail return . fromDot =<< readFile filepath 
  
   
-- | Read a graph from a dot file. Currently subgraphs are not supported at all.   
fromDot :: String -> Either String (Graph UnCALLabel)
fromDot str = fmap (reduce . removeDup) $ do
  dot  <- Dot.parseDot str
  D2GState vmap firstNode_ markedNode_ <- execStateT (go dot) (D2GState M.empty Nothing Nothing)
  let (imap, firstNode, markedNode) =
        (IM.fromAscList $ map (\(k,es) -> (toI k, map dstToI es)) $ M.toList vmap,
         fmap toI firstNode_, fmap toI markedNode_)
        where
          k2kmap :: M.Map Dot.ID Int 
          k2kmap = M.fromAscList (zip (M.keys vmap) [0..])
          toI    = fromJust . flip M.lookup k2kmap
          dstToI (Nothing, k) = EpsTo (toI k)
          dstToI (Just  l, k) = LabTo l (toI k)
  let rootCand =
        case reverse $ HsGraph.stronglyConnComp [ (k,k, map dst es) | (k,es) <- IM.toList imap ] of
          HsGraph.AcyclicSCC k:_ -> Just k
          _                      -> markedNode `mplus` firstNode
  case rootCand of
    Nothing -> fail "No root candidate" 
    Just  k -> return $ Graph k imap 
  where
    getLabel :: [Dot.Attribute] -> Maybe Dot.ID 
    getLabel [] = Nothing
    getLabel (Dot.Attr k v:attrs) | Dot.StringID "label" <- k = Just v
    getLabel (_:attrs) = getLabel attrs

    isMarked :: Dot.ID -> Bool
    isMarked (Dot.StringID s) = "{&}" `isPrefixOf` s
    isMarked _                = False


    storeNode :: Dot.ID -> Bool ->
                 StateT D2GState (Either String) ()
    storeNode n b = do 
      D2GState vmap fn mn <- get
      let vmap' = M.insertWith (++) n [] vmap 
      let fn'   = fn `mplus` Just n 
      let mn'   = if b then Just n else mn
      put $ D2GState vmap' fn' mn'

    storeEdge :: Dot.ID -> Maybe Dot.ID -> Dot.ID ->
                 StateT D2GState (Either String) ()
    storeEdge n l m = do
      storeNode n False
      storeNode m False 
      D2GState vmap fn mn <- get
      let vmap' = M.insertWith (++) n [(fmap toUnCALLabel l,m)] vmap 
      put $ D2GState vmap' fn mn
    
    go (Dot.Graph _ _ _ ss) = mapM_ goStmt ss

    goStmt (Dot.NodeStatement (Dot.NodeID n _) attrs) =
      storeNode n (maybe False isMarked $ getLabel attrs) 

    goStmt (Dot.EdgeStatement src ds attrs) = do 
      let l = getLabel attrs
      ns <- getNodes src
      mapM_ (\n -> mapM_ (goEdge l n) ds) ns

    goStmt _ = return () 
      
    getNodes (Dot.OfNode (Dot.NodeID n _)) = return [n]
    getNodes _                             = fail   "Subgraphs are not supported"

    goEdge l n (Dot.DirectedEdge d) = do 
      ms <- getNodes d
      mapM_ (storeEdge n l) ms
    goEdge l n (Dot.UnDirectedEdge d) = do 
      ms <- getNodes d
      mapM_ (\m -> storeEdge n l m >> storeEdge m l n) ms
      
      
toUnCALLabel :: Dot.ID -> UnCALLabel
toUnCALLabel (Dot.StringID "true")  = LBool True
toUnCALLabel (Dot.StringID "false") = LBool False
toUnCALLabel (Dot.StringID s@('"':_:_)) | last s == '"' = LString (read s)
toUnCALLabel (Dot.StringID s)       = LConst s
toUnCALLabel (Dot.IntID i)          = LInt (fromIntegral i)
toUnCALLabel (Dot.FloatID f)        = LFloat f 

  
  
   

      
-- | Applies `elminateEps` and `reduce` in this order. 
simplify :: Ord a => Graph a -> Graph a            
simplify = reduce . eliminateEps . removeDup

-- | Removes duplicated edges.
removeDup :: Ord a => Graph a -> Graph a
removeDup (Graph r vec) = Graph r vec'
  where
    vec' = IM.map (S.toList . S.fromList) vec 

-- | Eliminates epsilon edges. Currently, there is no need for users to use 
--   this function.  
eliminateEps :: Graph a -> Graph a
eliminateEps (Graph r vec) = resolveReachable $ 
  Graph r $ IM.fromListWith (++) $ 
    map (\(s,e,d) -> (s, [LabTo e d])) edges' 
  where
    resolveReachable (Graph r vec) =
      let us = concatMap (filter isNotKey . map dst . snd) $ IM.toList vec
      in Graph r $ foldr (\a -> IM.insertWith (++) a []) vec us
      where
        isNotKey k = not $ IM.member k vec 

    
    nonEpsEdges =
      [ (s,a,d) | (s,es) <- IM.toList vec, (LabTo a d) <- es ]
    epsEdges =
      [ (s,d) | (s,es) <- IM.toList vec, (EpsTo d) <- es ]

    epsFrom = IM.fromListWith (++) [ (d,[s]) | (s,d) <- epsEdges ]

    addPath [] _ _ _ trav = trav
    addPath (v:qs) e d looked trav
      | v `IS.member` looked = addPath qs e d looked trav
      | otherwise =
        case IM.lookup v epsFrom of
          Nothing -> addPath qs e d (IS.insert v looked) ((v,e,d):trav)
          Just ws -> addPath (ws++qs) e d (IS.insert v looked) ((v,e,d):trav)
 
    edges' = concatMap (\(s,e,d) -> addPath [s] e d IS.empty []) nonEpsEdges

-- | Removes unreachable parts from the root.
--   We call this function "reduce" because of the similarity to
--   reduction of tree automata. 
reduce :: Graph a -> Graph a
reduce (Graph r vec) = 
  let s = reachable [r] IS.empty
  in Graph r $ IM.filterWithKey (\k x -> IS.member k s) vec
  where
    reachable [] proced = proced
    reachable (q:qs) proced =
      if IS.member q proced then
        reachable qs proced
      else
        let ws = map dst $ fromMaybe [] $ IM.lookup q vec
        in reachable (ws++qs) (IS.insert q proced)

-- | Returns the number of vertices. 
vertexSize :: Graph a -> Int
vertexSize (Graph r vec) = IM.size vec 

-- | Returns the number of edges. 
edgeSize :: Graph a -> Int
edgeSize (Graph r vec) = sum (map (length . snd) $ IM.toList vec)

-- reduce :: Graph a -> Graph a 
-- reduce graph =
--   let (vs',es') = go [root graph] S.empty [] 
--   in   graph { vertices = S.toList vs', edges = reverse es' }
--   where
--     gmap = M.fromListWith (++) $ map (\(s,e,v) -> (s,[(e,v)])) (edges graph)
--     go [] vs es  = (vs,es)
--     go (q:qs) vs es 
--       | q `S.member` vs = go qs vs es
--       | otherwise   = 
--         case M.lookup q gmap of
--           Nothing  -> go qs (S.insert q vs) es
--           Just evs ->
--             go (map snd evs ++ qs) (S.insert q vs) ([ (q,e,d) | (e,d) <- evs ] ++ es)
