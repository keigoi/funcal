module Main where 

import Parser
import Lexer 
import Converter 
import System.Environment 
import System.IO 
import Text.PrettyPrint.HughesPJ

main = do { vs <- getArgs 
          ; s <- if null vs then getContents else readFile (head vs) 
          ; let v = parseExp s 
          ; let c = convert $ infer initEnv v
          ; putStrLn $ show $ nest 4 $ emitCode (toMonadicFUnCAL $ simplify c) }

                

