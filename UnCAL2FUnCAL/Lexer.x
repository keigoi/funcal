{
{-# OPTIONS_GHC -fno-warn-tabs #-}
module Lexer where 

import Syntax
}

%wrapper "posn"

$digit = 0-9
@digits = $digit+

$alpha = [a-zA-Z]
@letter  = ($alpha | $digit | "_" | ['])
@letters = @letter+
@single_marker_string = "&" @letter*
@marker_string        = @single_marker_string + 

@variable_string      = "$" @letter*
@newline = "\n" | "\r" | "\r\n"

-- $graphic    = $printable # $white
-- @string     = \" ($graphic # \")* \"

@string = \" ( ($printable # [\"\\]) | "\" "\" | "\n" | "\t" | "\r" | "\" [\"] )* \" --"
@char = \' ( ($printable # [\'\\]) | "\" "\" | "\n" | "\t" | "\r" | "\" [\"] ) \' --"
@backslash = "\" --"

@comment = ([^\*]+ | $white+ | ("*" [^\)]) )*

tokens :- 
       $white+ ;
       "(*" @comment "*)";
       "//".* ;
       @string               { wrap $ \p s -> TkString p (read s) }
       "doc"                 { wrap $ TkKey }
       "true"                { wrap $ \p s -> TkBool p True }
       "false"               { wrap $ \p s -> TkBool p False }
       "cycle"               { wrap $ TkKey }
       "U"                   { wrap $ TkSym }
       "rec"                 { wrap $ TkKey }
       "if"                  { wrap $ TkKey }
       "then"                { wrap $ TkKey }
       "else"                { wrap $ TkKey }
       "llet"                { wrap $ TkKey }
       "let"                 { wrap $ TkKey }
       "in"                  { wrap $ TkKey }
       "isempty"             { wrap $ TkKey }
       "not"                 { wrap $ TkKey }
       "and"                 { wrap $ TkKey }
       "or"                  { wrap $ TkKey }
       "@"                   { wrap $ TkSym }
       "@@"                  { wrap $ TkSym }
       ":"                   { wrap $ TkSym }
       ","                   { wrap $ TkSym } 
       "."                   { wrap $ TkSym }
       "{"                   { wrap $ TkSym }
       "}"                   { wrap $ TkSym }
       "("                   { wrap $ TkSym }
       ")"                   { wrap $ TkSym }
       ":="                  { wrap $ TkSym }
       @backslash            { wrap $ TkSym }
       $digit+               { wrap $ \p s -> TkNumber p (read s) } 
       @marker_string        { wrap $ TkMarker }
       @variable_string      { wrap $ TkIdent  }    
       @letters              { wrap $ TkLetters }
       [\=\+\-\*\/\<\>\$\|\?\!] { wrap $ TkSym }


{
data Token a = TkSym a String 
             | TkKey a String 
             | TkBool a Bool 
             | TkIdent a String 
             | TkString a String 
             | TkNumber a Int 
             | TkMarker a String 
             | TkLetters a String 

instance Show (Token a) where 
    show (TkSym _ s) = s 
    show (TkKey _ s) = s 
    show (TkBool _ s) = show s 
    show (TkIdent _ s) = s
    show (TkMarker _ s) = s 
    show (TkString _ s) = show s 
    show (TkNumber  _ s) = show s 
    show (TkLetters _ s) = s 


--     showList ss rest = foldr (\a r -> show a ++ " " ++ r) rest ss 

instance ExtraInfo Token where 
    info (TkSym a _)    = a
    info (TkKey a _)    = a 
    info (TkBool a _)   = a 
    info (TkIdent a _)  = a 
    info (TkString a _) = a 
    info (TkNumber a _) = a 
    info (TkMarker a _) = a 
    info (TkLetters a _) = a 
        
 
wrap f (AlexPn off l c) s
      = f (SourceRange (off,l,c) (off + length s, l, c + length s)) s
}
