module Converter where 

import Syntax 
import qualified Data.Set as S 

import Data.Maybe 
import Data.List 
import Control.Monad.State 

import Text.PrettyPrint.HughesPJ 

import Debug.Trace 

data Ty = TyGraph (S.Set Name) (S.Set Name)
        | TyOther Bool 

instance Show Ty where 
    show (TyGraph xs ys) = 
        "DB^" ++ show (S.toList xs) ++ "_" ++ show (S.toList ys) 
    show (TyOther _) =
        "_"


defaultM :: Name
defaultM = MName "&"

dM :: S.Set Name
dM = S.singleton defaultM 

tyinfo :: IExp (a,Ty) -> Ty 
tyinfo = snd . info 

tyerror :: (Show a2, Show a1) => a1 -> a2 -> a
tyerror x e = error $ "Type Error @ " ++ show x ++ " " ++ show e 


extend :: Name -> Name -> Name
extend (MName "&") x  = x 
extend x  (MName "&") = x 
extend (MName x) (MName y) = MName $ x ++ y 

extends :: S.Set Name -> S.Set Name -> S.Set Name
extends xs ys = S.fromList [ extend x y | x <- S.toList xs, y <- S.toList ys ]

initEnv :: [(Name, Ty)]
initEnv = [(Name "$db", TyGraph dM S.empty) ]

infer :: Show a => [(Name,Ty)] -> IExp a -> IExp (a, Ty) 
infer env (Info x e) = -- (\s -> trace (">>> " ++ show e ++ "\n--> " ++ show s ++ "\n") s) $ 
    case e of 
      ECons e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2
          in case (tyinfo e1', tyinfo e2') of
               (TyOther _, TyGraph xs2 ys2) | xs2 == dM -> 
                      Info (x,TyGraph dM ys2) $ ECons e1' e2' 
               _ -> tyerror x e
      EEmp       -> Info (x,TyGraph dM S.empty) EEmp 
      EUni e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2
          in case (tyinfo e1', tyinfo e2') of
               (TyGraph xs1 ys1, TyGraph xs2 ys2) 
                   | xs1 == xs2 -> 
                       Info (x,TyGraph xs1 (S.union ys1 ys2)) (EUni e1' e2')
               _ ->
                   tyerror x e
      ENull      -> Info (x,TyGraph S.empty S.empty) ENull 
      EDUni e1 e2 ->
          let e1' = infer env e1 
              e2' = infer env e2 
          in case (tyinfo e1', tyinfo e2') of 
               (TyGraph xs1 ys1, TyGraph xs2 ys2) 
                   | S.null (S.intersection xs1 xs2) ->
                       Info (x,TyGraph (S.union xs1 xs2) (S.union ys1 ys2)) (EDUni e1' e2')
               _ -> tyerror x e
      EIMark n e -> 
          let e' = infer env e 
          in case tyinfo e' of 
               TyGraph xs ys -> 
                   Info (x,TyGraph (S.map (extend n) xs) ys) $ EIMark n e' 
               _ -> tyerror x e
      EOMark n -> 
          Info (x,TyGraph dM (S.singleton n)) $ EOMark n 
      EAt e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2 
          in case (tyinfo e1', tyinfo e2') of 
               (TyGraph xs ys, TyGraph zs ws) | S.isSubsetOf ys zs ->
                   Info (x,TyGraph xs ws) $ EAt e1' e2' 
               _ -> tyerror x e
      ECycle e -> 
          let e' = infer env e
          in case tyinfo e' of 
               TyGraph xs ys -> 
                   Info (x,TyGraph xs (S.difference ys xs)) $ ECycle e' 
      EDoc s   -> Info (x,TyGraph dM S.empty) $ EDoc s 
      EIf e1 e2 e3 ->
          let e1' = infer env e1 
              e2' = infer env e2
              e3' = infer env e3
          in case (tyinfo e1', tyinfo e2', tyinfo e3') of 
               (TyOther _, TyGraph xs2 ys2, TyGraph xs3 ys3) 
                   | xs2 == xs3 ->
                       Info (x,TyGraph xs2 (S.union ys2 ys3)) $ EIf e1' e2' e3'
               _ -> tyerror x e
      ERec (l,g,e1) e2 ->
          let Info (x1,t) eb1' = infer ((l,TyOther False):(g,TyGraph dM S.empty):env) e1 
              e2' = infer env e2 
          in case (t,tyinfo e2') of 
               (TyGraph xs1 ys1, TyGraph xs2 ys2) 
                  | S.isSubsetOf ys1 xs1 && S.null xs2 && S.null ys2 ->
                      Info (x, TyGraph S.empty S.empty) ENull 
                  | S.isSubsetOf ys1 xs1 && S.null ys2 -> 
                      foldr1 (\a b -> 
                                  let TyGraph axs ays = tyinfo a 
                                      TyGraph bxs bys = tyinfo b 
                                  in Info (x, TyGraph (S.union axs bxs) (S.union ays bys)) $ 
                                          EDUni a b) $ 
                      do { m <- S.toList xs2 
                         ; let e2m = Info (x, TyGraph (S.singleton m) ys2) $
                                       (EIMark m (Info (x, TyGraph dM ys2) $
                                                   EAt (Info (x, TyGraph dM (S.singleton m)) $ EOMark m)
                                                       e2'))
                         ; return $ Info (x,TyGraph (S.map (\x -> extend x m) xs1) S.empty) $
                               ERec (l,g,Info (x1, TyGraph xs1 ys1) eb1') e2m }
                      -- Info (x,TyGraph (extends xs1 xs2) S.empty) $ 
                      --      ERec (l,g,Info (x1,TyGraph xs1 ys1) eb1') e2'
               _ -> tyerror x e
      ELet n e1 e2 ->
          let e1' = infer env e1 
              e2' = infer ((n,tyinfo e1'):env) e2 
          in Info (x,tyinfo e2') $ ELet n e1' e2' 
      EEq e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2 
          in case (tyinfo e1', tyinfo e2') of 
               (TyOther b1, TyOther b2) -> Info (x,TyOther (b1 || b2)) $ EEq e1' e2' 
               _                  -> tyerror x e
      EVar n -> 
          let t = fromJust $ lookup n env 
          in Info (x,t) (EVar n) 
      ELabel n -> 
          Info (x,TyOther False) (ELabel n) 
      EBool b -> 
          Info (x,TyOther False) (EBool b)
      EAnd e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2 
          in case (tyinfo e1', tyinfo e2') of 
               (TyOther b1, TyOther b2) -> Info (x,TyOther (b1 || b2)) $ EAnd e1' e2' 
               _                        -> tyerror x e 
      EOr e1 e2 -> 
          let e1' = infer env e1 
              e2' = infer env e2 
          in case (tyinfo e1', tyinfo e2') of 
               (TyOther b1, TyOther b2) -> Info (x,TyOther (b1 || b2)) $ EOr e1' e2' 
               _                        -> tyerror x e 
      ENot e1 -> 
          let e1' = infer env e1 
          in case tyinfo e1' of 
               TyOther b -> Info (x,TyOther b) $ ENot e1' 
               _         -> tyerror x e 
      EIsEmpty e -> 
          let e' = infer env e 
          in case tyinfo e' of 
               TyGraph _ _ -> 
                   Info (x,TyOther True) $ EIsEmpty e' 
               _           -> tyerror x e 

data FTy = FTyArr   FTy FTy 
         | FTyGraph Int 
         | FTyOther Bool 

instance Show FTy where 
    show x = "(" ++ go x ++ ")"
        where
          go (FTyArr x1 x2) = 
              goP x1 ++ " --> " ++ go x2 
          go (FTyGraph i) 
             | i == 0 = "()"
             | i == 1 = "G" 
             | otherwise = 
              "G^"++ show i 
          go (FTyOther _) = "_" 
          goP (x@(FTyArr _ _)) = "(" ++ go x ++ ")"
          goP x = go x 

infixr 3 --> 
(-->) :: FTy -> FTy -> FTy
(-->) = FTyArr 

                   
convertT :: Ty -> FTy
convertT (TyGraph xs ys) = FTyArr (FTyGraph (S.size ys)) (FTyGraph (S.size xs))
convertT (TyOther b)     = FTyOther b
                              
graphTypes :: Int -> FTy
graphTypes = FTyGraph 

gT0 :: FTy
gT0 = graphTypes 0 

gT1 :: FTy
gT1 = graphTypes 1 

iFAbs :: a -> Name -> IFExp a1 -> Info (FExp a1) a
iFAbs i n  e   = Info i (FAbs n e)

iFApp :: Info (FExp FTy) FTy -> IFExp FTy -> Info (FExp FTy) FTy
iFApp e1 e2    = case info e1 of 
                   FTyArr _ i -> Info i (FApp e1 e2) 
--                    FTyGraph _ -> e1 -- not essential solution 
                   _  -> error $ "Error: " ++ show e1 ++ "\n" ++ show e2

iFCons :: IFExp a -> IFExp a -> Info (FExp a) FTy
iFCons e1 e2   = Info (graphTypes 1) $ FCons e1 e2 

iFVar :: a -> Name -> Info (FExp a1) a
iFVar i n      = Info i (FVar n)

iFTup :: [IFExp a] -> Info (FExp a) FTy
iFTup es       = Info (graphTypes $ length es) (FTup es)

iFEmp :: Info (FExp a) FTy
iFEmp          = Info (graphTypes 1) FEmp 

iFPrj :: Int -> Int -> Info (FExp a) FTy
iFPrj n i      = Info (graphTypes n --> graphTypes 1) $ FPrj n i 

iFUni :: IFExp a -> IFExp a -> Info (FExp a) FTy
iFUni e1 e2    = Info (graphTypes 1) $ FUni e1 e2 

iFIf :: IFExp a -> Info (FExp a) a -> IFExp a -> Info (FExp a) a
iFIf e1 e2 e3  = Info (info e2) $ FIf e1 e2 e3 



convert :: IExp (a,Ty) -> IFExp FTy 
convert ie = evalState (go ie) 0 
    where 
      newVar = do { i <- get; put (i+1); return $ IName i }
      newVars n = mapM (\_ -> newVar) [1..n]
      go (Info (x,t) e) = 
          case e of 
            ECons e1 e2 -> 
                do { e1' <- go e1 
                   ; e2' <- go e2 
                   ; y   <- newVar 
                   ; let FTyArr sT dT = convertT t 
                   ; case sT of 
                       FTyGraph 0 -> 
                           return $ iFCons e1' e2' 
                       _ -> 
                           return $ iFAbs (convertT t) y $ 
                                  iFCons e1' (iFApp e2' (iFVar sT y)) }
            EEmp -> 
                case convertT t of 
                  FTyArr (FTyGraph 0) _ ->
                      return $ iFEmp 
                  -- _ -> -- can't be taken 
                  --     do { y <- newVar 
                  --        ; return $ iFAbs (convertT t) y iFEmp }
            EUni e1 e2 -> 
                do { e1' <- go e1 
                   ; e2' <- go e2 
                   ; let TyGraph xs ys  = t 
                   ; let TyGraph xs1  ys1 = tyinfo e1 
                   ; let TyGraph xs2  ys2 = tyinfo e2 
                   ; y <- newVar
                   ; let yT = graphTypes (S.size ys) 
                   ; let dT  = graphTypes (S.size xs) 
                   ; let sT  = graphTypes (S.size ys) 
                   ; w1 <- newVar -- of type dT
                   ; w2 <- newVar -- of type dT 
                   ; let e1''= if S.size ys1 == 0 then 
                                   e1' 
                               else 
                                   iFApp e1' (selectP (S.toList ys) (S.toList ys1) (iFVar sT y))
                   ; let e2''= if S.size ys2 == 0 then 
                                   e2'
                               else 
                                   iFApp e2' (selectP (S.toList ys) (S.toList ys2) (iFVar sT y))
                   ; let arr = iFAbs (dT --> dT --> dT) w1 $
                                 iFAbs (dT --> dT) w2 $ 
                                   iFTup [iFUni (iFApp (iFPrj (S.size xs) i)
                                                       (iFVar dT w1))
                                                (iFApp (iFPrj (S.size xs) i)
                                                       (iFVar dT w2))
                                                    | i <- [1..S.size xs] ]
                   ; if S.size ys == 0 then 
                         return $ iFApp (iFApp arr e1'') e2'' 
                     else 
                         return $ iFAbs (sT --> dT) y $ 
                                 iFApp (iFApp arr e1'') e2'' }
            ENull ->
                case convertT t of 
                  FTyArr (FTyGraph 0) _ -> 
                      return $ iFTup []
                  -- _ -> -- can't be taken 
                  --     do { y <- newVar 
                  --        ; return $ iFAbs (convertT t) y $ iFTup [] }
            EDUni e1 e2 -> 
                do { e1' <- go e1 
                   ; e2' <- go e2 
                   ; let TyGraph xs   ys  = t 
                   ; let TyGraph xs1  ys1 = tyinfo e1 
                   ; let TyGraph xs2  ys2 = tyinfo e2 
                   ; let sT = graphTypes (S.size ys) 
                   ; let dT = graphTypes (S.size xs) 
                   ; y <- newVar 
                   ; w1 <- newVar 
                   ; w2 <- newVar 
                   ; let w1T = graphTypes (S.size xs1) 
                   ; let w2T = graphTypes (S.size xs2) 
                   ; let e1'' = if S.size ys1 == 0 then 
                                    e1' 
                                else 
                                    iFApp e1' (selectP (S.toList ys) (S.toList ys1) (iFVar sT y))
                   ; let e2'' = if S.size ys2 == 0 then 
                                    e2' 
                                else
                                    iFApp e2' (selectP (S.toList ys) (S.toList ys2) (iFVar sT y))
                   ; let arr  = iFAbs (w1T --> w2T --> dT) w1 $
                                  iFAbs (w2T --> dT) w2 $ 
                                      arrangeP (S.toList xs1) (S.toList xs2) (S.toList xs) 
                                               (iFVar w1T w1) (iFVar w2T w2)
                   ; if S.size ys == 0 then 
                         return $ iFApp (iFApp arr e1'') e2'' 
                     else 
                         return $ iFAbs (convertT t) y $ 
                                iFApp (iFApp arr e1'') e2''}
            EIMark m e -> go e 
            EOMark m   ->
                do { y <- newVar 
                   ; return $ iFAbs (convertT t) y (iFVar (graphTypes 1) y)}
            EAt e1 e2 ->
                do { e1' <- go e1
                   ; e2' <- go e2 
                   ; let TyGraph _ ys1  = tyinfo e1 
                   ; let TyGraph xs2 ys = tyinfo e2 
                   ; y <- newVar 
                   ; w <- newVar 
                   ; let dT2 = graphTypes (S.size xs2) 
                   ; let sT1 = graphTypes (S.size ys1)
                   ; let FTyArr sT dT = convertT t 
                   ; let e1'' = iFAbs (dT2 --> sT1) w $ 
                                   iFApp e1' (selectP (S.toList xs2) (S.toList ys1) (iFVar dT2 w))
                   ; if S.size ys1 == 0 then 
                         return $ e1'
                     else if S.size ys == 0 then 
                              return $ iFApp e1'' e2'
                          else 
                              return $ iFAbs (convertT t) y 
                                         $ (iFApp e1'' (iFApp e2' (iFVar sT y)))}
            ECycle e1-> 
               do { e1' <- go e1
                  ; let TyGraph xs1 ys1 = tyinfo e1 
                  ; let TyGraph xs   ys  = t -- ys1 = xs1 U ys
                  ; let FTyArr sT dT = convertT t 
                  ; y <- newVar 
                  ; w <- newVar -- dT
                  ; let sT1 = graphTypes (S.size ys1) 
                  ; let dT1 = dT 
                  ; let f = iFApp e1' (arrangeP (S.toList xs1) (S.toList ys) (S.toList ys1) (iFVar dT w) (iFVar sT y))
                  ; if S.size xs == 0 then 
                        if S.size ys == 0 then 
                            return $ iFTup []
                        else
                            return $ iFAbs (sT --> dT) y $ iFTup []
                    else if S.size ys == 0 then 
                             return (Info dT $ FFix (S.size xs1) (iFAbs (dT --> dT) w f))
                         else
                             return $ iFAbs (sT --> dT) y $ 
                                        (Info dT $ FFix (S.size xs1) (iFAbs (dT --> dT) w f)) }
            EDoc s -> 
                case convertT t of 
                  FTyArr (FTyGraph 0) _ -> 
                      return $ Info gT1 (FDoc s)
                  -- _ -> -- can't be taken 
                  --     do { y <- newVar 
                  --        ; return $ iFAbs (convertT t) y $ 
                  --                 Info (graphTypes 1) (FDoc s) }
            EIf e1 e2 e3 -> 
                do { e1' <- go e1 
                   ; e2' <- go e2
                   ; e3' <- go e3 
                   ; let TyGraph _ ys = t
                   ; let TyGraph _ ys2 = tyinfo e2 
                   ; let TyGraph _ ys3 = tyinfo e3
                   ; let sT = graphTypes (S.size ys) 
                   ; y <- newVar 
                   ; let e2'' = if S.size ys2 == 0 then 
                                    e2' 
                                else 
                                    iFApp e2' (selectP (S.toList ys) (S.toList ys2) (iFVar sT y))
                   ; let e3'' = if S.size ys3 == 0 then 
                                    e3'
                                else 
                                    iFApp e3' (selectP (S.toList ys) (S.toList ys3) (iFVar sT y)) 
                   ; if S.size ys == 0 then 
                         return $ iFIf e1' e2'' e3'' 
                     else 
                         return $ iFAbs (convertT t) y $ 
                                (iFIf e1' e2'' e3'') }
            ERec (l,g,e1) e2 -> 
                do { let TyGraph xs1 ys1 = tyinfo e1 
                   ; let TyGraph xs2 _   = tyinfo e2 
                   ; e1' <- go e1
                   ; e2' <- go e2 
                   ; gx  <- newVar 
                   ; tmp <- newVar 
                   ; w   <- newVar 
                   ; let zT = graphTypes (S.size xs1) 
                   ; let dT = graphTypes (S.size xs1 * S.size xs2) 
                   ; let e1'' = if S.size ys1 == 0 then 
                                    iFAbs (zT --> zT) w $ e1' 
                                        
                                else
                                    iFAbs (zT --> zT) w $ 
                                          iFApp e1' (selectP (S.toList xs1) (S.toList ys1) (iFVar zT w))
                   -- ; let f    = iFAbs (FTyOther False --> gT1 --> zT --> zT) l $ 
                   --                (iFAbs (gT1 --> zT --> zT) gx $ 
                   --                    (iFApp (iFAbs ((gT0 --> gT1) --> zT --> zT) g $ e1'') 
                   --                           (iFAbs (gT0 --> gT1) tmp $ (iFVar (graphTypes 1) gx ))))
                   ; let f    = iFAbs (FTyOther False --> gT1 --> zT --> zT) l $ 
                                  (iFAbs (gT1 --> zT --> zT) g $  e1'')
                   ; let para   = Info (gT1 --> zT) $ FPara (S.size xs1) f 
                   ; y <- newVar 
                   ; case convertT t of 
                       FTyArr (FTyGraph 0) _ ->
                           return $ iFApp para e2' 
                       _ -> trace ("Oh!") $ 
                           return $ iFAbs (convertT t) y $
                                 iFApp para (iFApp e2' (iFVar gT1 y)) }
            ELet n e1 e2 ->
                do { e1' <- go e1
                   ; e2' <- go e2
                   ; let e1T = info e1' 
                   ; let e2T = info e2' 
                   ; return $ iFApp (iFAbs (e1T --> e2T) n e2') e1' }
            EEq e1 e2 ->
                do { e1' <- go e1 
                   ; e2' <- go e2 
                   ; return $ Info (convertT t) $ FEq e1' e2' }
            EVar x ->
                case convertT t of
                  FTyArr (FTyGraph 0) dT -> 
                      return $ Info dT $ FVar x 
                  _ -> 
                      return $ Info (convertT t) $ FVar x 
            ELabel l -> 
                return $ Info (convertT t) $ FLabel l 
            EBool  b -> 
                return $ Info (convertT t) $ FBool b 
            EAnd e1 e2 -> 
                do { e1' <- go e1
                   ; e2' <- go e2 
                   ; return $ Info (convertT t) $ FAnd e1' e2' }
            EOr e1 e2 -> 
                do { e1' <- go e1
                   ; e2' <- go e2 
                   ; return $ Info (convertT t) $ FOr e1' e2' }
            ENot e1  -> 
                do { e1' <- go e1
                   ; return $ Info (convertT t) $ FNot e1' }
            EIsEmpty e1 -> 
                do { e1' <- go e1
                   ; let TyGraph xs1 ys1 = tyinfo e1 
                   ; case S.toList xs1 of 
                       []   -> return $ Info (FTyOther False) $ FBool True
                       lxs1 -> 
                           do { let tt = Info (FTyOther False) $ FBool True 
                              ; let ff = Info (FTyOther False) $ FBool False 
                              ; let myand x y = iFIf x (iFIf y tt ff) ff
                              ; let arg = iFTup [ iFEmp | _ <- S.toList ys1 ]
                              ; let e1''= if S.size ys1 == 0 then 
                                              e1'
                                          else
                                              iFApp e1' arg 
                              ; let res = case lxs1 of 
                                            [m] -> Info (FTyOther True) $ FIsEmpty $ 
                                                     e1''
                                            _   -> foldr myand tt $
                                                   [ Info (FTyOther True) $ FIsEmpty $
                                                          iFApp (iFPrj (length lxs1) i)
                                                                    e1''
                                                                        | i <- [1..length lxs1]]
                              ; return res }}
                                        


selectP :: Eq a => [a] -> [a] -> IFExp FTy -> IFExp FTy
selectP xs ys k = 
    let l = length xs in 
    iFTup [ iFApp (iFPrj l (fromJust $ elemIndex y xs)) k | y <- ys ]

arrangeP :: Eq a => [a] -> [a] -> [a] -> IFExp FTy -> IFExp FTy -> IFExp FTy
arrangeP xs1 xs2 xs k1 k2 = 
    iFTup [ check x | x <- xs ]
        where
          l1 = length xs1 
          l2 = length xs2 
          check x = case elemIndex x xs1 of
                      Just i -> iFApp (iFPrj l1 i) k1 
                      Nothing -> 
                          case elemIndex x xs2 of 
                            Just i -> iFApp (iFPrj l2 i) k2 
                       
----
                
freeVars :: IFExp t -> [Name]
freeVars e = go e [] []
    where
      go (Info _ e) bound r =
          case e of 
            FCons e1 e2 -> go e2 bound (go e1 bound r)
            FUni  e1 e2 -> go e2 bound (go e1 bound r) 
            FTup  es    -> foldl (\r a -> go a bound r) r es 
            FApp e1 e2 -> go e2 bound (go e1 bound r)
            FAbs x e   -> go e (x:bound) r 
            FAbsT ns e -> go e (ns ++ bound) r 
            FVar x     -> if elem x bound then r else x:r 
            FFix _ f   -> go f bound r 
            FPara _ f  -> go f bound r 
            FIf e1 e2 e3 -> go e3 bound (go e2 bound (go e1 bound r))
            FEq e1 e2  -> go e2 bound (go e1 bound r)
            FIsEmpty e -> go e bound r 
            FAnd e1 e2 -> go e2 bound (go e1 bound r)
            FOr  e1 e2 -> go e2 bound (go e1 bound r)
            FNot e1    -> go e1 bound r 
            _          -> r 

doesOccurFree :: Name -> IFExp t -> Bool
doesOccurFree x e = x `elem` freeVars e

doesOccurOnce :: Name -> IFExp t -> Bool
doesOccurOnce x e = 
    case filter (==x) $ freeVars e of 
      [_] -> True 
      _   -> False

replace :: Name -> Info (FExp a) t -> IFExp a -> IFExp a
replace y t = repl
    where 
      body (Info _ x) = x 
      repl (Info t x) = Info t (go x)
      go e = 
          case e of 
            FCons e1 e2  -> FCons (repl e1) (repl e2) 
            FUni  e1 e2  -> FUni  (repl e1) (repl e2)
            FTup  es     -> FTup  (map repl es)
            FApp e1 e2   -> FApp  (repl e1) (repl e2)
            FAbs x e     -> if x == y then FAbs x e else FAbs x (repl e)
            FAbsT xs e   -> if y `elem` xs then FAbsT xs e else FAbsT xs (repl e)
            FVar x       -> if x == y then body t else FVar x 
            FFix i f     -> FFix i (repl f)
            FPara i f    -> FPara i (repl f)
            FIf e1 e2 e3 -> FIf (repl e1) (repl e2) (repl e3)
            FEq e1 e2    -> FEq (repl e1) (repl e2)
            FAnd e1 e2   -> FAnd (repl e1) (repl e2)
            FOr  e1 e2   -> FOr  (repl e1) (repl e2)
            FNot e1      -> FNot (repl e1)
            FIsEmpty e   -> FIsEmpty (repl e)
            x            -> x 



doesOccurUnderPrj :: Name -> IFExp t -> Bool
doesOccurUnderPrj y e = go e 
    where
      go (Info _ e) =
          case e of 
            FCons e1 e2  -> go e1 && go e2
            FUni  e1 e2  -> go e1 && go e2
            FTup  es     -> all go es
            FApp e1 e2   -> 
                case (e1,e2) of 
                  (Info _ (FPrj _ _), Info _ (FVar x)) 
                             | x == y -> True
                  _ ->
                      go e1 && go e2
            FAbs x e     -> if x == y then True  else go e
            FAbsT xs e   -> if y `elem` xs then True else go e 
            FVar x       -> if x == y then False else True
            FFix i f     -> go f 
            FPara i f    -> go f 
            FIf e1 e2 e3 -> go e1 && go e2 && go e3 
            FEq e1 e2    -> go e1 && go e2 
            FAnd e1 e2   -> go e1 && go e2
            FOr  e1 e2   -> go e1 && go e2 
            FNot e1      -> go e1 
            FIsEmpty e   -> go e 

            x            -> True

replacePrj :: Name -> [Info (FExp a) t1] -> Info (FExp a) a -> Info (FExp a) a
replacePrj y es e = repl e
    where
      repl (Info i e) = Info i (go e)
      go e =
          case e of 
            FCons e1 e2 -> FCons (repl e1) (repl e2)
            FUni  e1 e2 -> FUni  (repl e1) (repl e2)
            FTup  es    -> FTup  (map repl es)
            FApp e1 e2  ->
                case (e1,e2) of 
                  (Info _ (FPrj n i), Info _ (FVar x))
                      | x == y -> 
                          body $ es !! i
                  _ -> 
                      FApp (repl e1) (repl e2)
            FAbs x e -> if x == y then FAbs x e else FAbs x (repl e)
            FAbsT xs e -> if y `elem` xs then FAbsT xs e else FAbsT xs (repl e) 
            FVar x   -> FVar x 
            FFix i f -> FFix i (repl f)
            FPara i f -> FPara i (repl f)
            FIf e1 e2 e3 -> FIf (repl e1) (repl e2) (repl e3)
            FEq e1 e2    -> FEq (repl e1) (repl e2)
            FAnd e1 e2   -> FAnd (repl e1) (repl e2)
            FOr  e1 e2   -> FOr  (repl e1) (repl e2)
            FNot e1      -> FNot (repl e1)
            FIsEmpty e   -> FIsEmpty (repl e)
            x            -> x 

-- simplify :: IFExp a -> IFExp a       
simplify :: IFExp a -> IFExp a
simplify eorg@(Info ty e) = 
    Info ty $ 
         case e of 
             FEmp -> FEmp 
             FCons e1 e2 -> 
                 FCons (simplify e1) (simplify e2)
             FUni  e1 e2 ->
                 case (simplify e1, simplify e2) of 
                   (Info _ FEmp, e2') -> body e2' 
                   (e1', Info _ FEmp) -> body e1'
                   (e1',  e2') -> FUni e1' e2' 
             FTup [e] -> body $ simplify e 
             FTup es  -> FTup $ map simplify es 
             FPrj n i -> FPrj n i 
             FAbsT ns e -> FAbsT ns (simplify e)     
             FAbs n e -> 
               case simplify e of 
                 Info _ (FApp e' (Info _ (FVar y))) 
                   | n == y && not (doesOccurFree y e) ->
                     body $ simplify e' 
                 e' -> 
                   FAbs n e'
             FVar x   -> FVar x 
             FFix i f -> FFix i (simplify f)
             FPara i f -> FPara i (simplify f)
             FDoc s    -> FDoc s
             FBool b   -> FBool b 
             FIf e1 e2 e3 -> FIf (simplify e1) (simplify e2) (simplify e3)
             FEq e1 e2    -> FEq (simplify e1) (simplify e2)
             FAnd e1 e2   -> 
                 case (simplify e1, simplify e2) of 
                   (Info _ (FBool True),  e2') -> body e2'
                   (Info _ (FBool False), e2') -> FBool False 
                   (e1', Info _ (FBool True))  -> body e1' 
                   (e1', Info _ (FBool False)) -> FBool False  
                   (e1',e2') -> FAnd e1' e2' 
             FOr  e1 e2   -> 
                 case (simplify e1, simplify e2) of 
                   (Info _ (FBool True),  e2') -> FBool True
                   (Info _ (FBool False), e2') -> body e2' 
                   (e1', Info _ (FBool True))  -> FBool True 
                   (e1', Info _ (FBool False)) -> body e1' 
                   (e1',e2') -> FOr e1' e2'
             FNot e1      -> 
                 case simplify e1 of 
                   (Info _ (FBool b)) -> FBool (not b) 
                   e1'                -> FNot e1' 
             FLabel l     -> FLabel l 
             FIsEmpty e   -> FIsEmpty (simplify e)
             FApp e1 e2 ->
                 case simplify e1 of 
                   e1'@(Info _ (FAbs n e11))
                       | not (doesOccurFree n e11) ->
                         body $ e11
                       | doesOccurOnce n e11 ->
                         body $ simplify $ replace n e2 e11
                       | otherwise -> 
                           case simplify e2 of 
                             e2'@(Info _ (FTup es)) -> 
                                 if doesOccurUnderPrj n e11 then
                                   -- (\x -> ... fst x ... snd y ...) (a,b) ---> ... a ... b ...
                                   body $ replacePrj n es e11 
                                 else
                                     FApp e1' e2' 
                             e2' -> 
                               if simpleEnough e2' then 
                                 body $ replace n e2' e11
                               else 
                                 FApp e1' e2'
                   e1'@(Info _ (FAbsT ns e11)) ->
                       case simplify e2 of
                         e2'@(Info _ (FTup es)) ->
                           body $ foldr (\(n,e) r -> replace n e r) e11 $ zip ns es
                         e2'->
                           FApp e1' e2' 
                   -- e1'@(Info (FTyArr (FTyGraph 0) _) (FVar _)) ->
                   --     body $ simplify e1'
                   Info _ (FPrj 1 _) -> 
                       body $ simplify e2
                   e1' -> 
                       FApp e1' (simplify e2)
    where
      simpleEnough (Info _ (FVar v)) = True 
      simpleEnough (Info _ (FApp (Info _ (FPrj _ _)) (Info _ (FVar v)))) = True 
      simpleEnough (Info _ FEmp) = True 
      simpleEnough (Info _ (FBool _)) = True 
      simpleEnough (Info _ (FDoc _)) = True 
      simpleEnough (Info _ (FLabel _)) = True 
      simpleEnough _ = False 
                          

toMonadicFUnCAL :: IFExp FTy -> TargetExp
toMonadicFUnCAL e = evalState (eraseFun $ go [] e) 0 
  where
    newName = do i <- get
                 put (i+1)
                 return $ IName' i
    
    wrapFixBody f = 
      TEFun $ \x -> (app f (TEReturn x))

    wrapParaBody f = 
      TEFun $ \x -> TEFun $ \y -> TEFun $ \z -> 
      app (app (app f (TEReturn x)) (TEReturn y)) (TEReturn z)


    app (TEFun f) e = f e
    app (TEFun' n f) e | isSimpleEnough e = f e 
    app e1 e2       = TEApp e1 e2

    bind (TEReturn e1)   e2 = app e2 e1
    bind (TESequence e1) (TEProj n i) = e1 !! i
    bind e1 e2             = TEBind e1 e2 

    makeMConcat (TEMConcat es) (TEMConcat es') = TEMConcat (es++es')
    makeMConcat (TEMConcat es) e'              = TEMConcat (es ++ [e'])
    makeMConcat e              (TEMConcat es') = TEMConcat (e:es')
    makeMConcat e              e'              = TEMConcat [e,e']

    go env (Info t e) = go_ env t e
    go_ env t e = case e of
      FEmp -> TEEnd
      FCons e1 e2 ->
        bind (go env e1) $ TEFun $ \x -> 
        bind (go env e2) (app TECons x)
      FUni e1 e2  -> 
        makeMConcat (go env e1) (go env e2)
      FTup es     -> TESequence $ map (go env) es
      FPrj n i    -> 
        TEFun $ \x -> bind x (TEFun $ \y -> TEReturn (app (TEProj n i) y))
      FApp e1 e2 ->
        case info e2 of
          FTyGraph n | n > 0 ->
            -- sharing 
            bind (go env e2) (TEFun $ \x -> app (go env e1) (TEReturn x))
          _ -> 
            app (go env e1) (go env e2)
      FAbs n e ->
        TEFun' n (\x -> go ((n,x):env) e)
      FAbsT ns e -> 
        TEFun $ \x -> (bind x (TEAbsT ns (go env e)))
      FVar  x   ->
        case lookup x env of
          Just e' -> e' 
          Nothing -> TEVar x
      FFix n e  -> 
        let e' = wrapFixBody (go env e)
        in (app (TEFix n) e')
      FPara n e -> 
        let e' = wrapParaBody (go env e)
        in TEFun $ \x -> bind x (app (TEPara n) e')
      FDoc s -> TEDoc s
      FIf e1 e2 e3 ->
        bind (go env e1) $ TEFun $ \x -> 
        TEIf x (go env e2) (go env e3)
      FEq e1 e2 ->
        bind (go env e1) $ TEFun $ \x ->
        bind (go env e2) $ TEFun $ \y -> 
        TEReturn (TEBin OpEq x y)
      FLabel l  -> TEReturn (TELabel l)
      FBool  b  -> TEReturn (TEBool b)
      FAnd   e1 e2 ->
        bind (go env e1) $ TEFun $ \x ->
        bind (go env e2) $ TEFun $ \y -> 
        TEReturn (TEBin OpAnd x y)
      FOr    e1 e2 ->
        bind (go env e1) $ TEFun $ \x ->
        bind (go env e2) $ TEFun $ \y -> 
        TEReturn (TEBin OpOr x y)
      FNot   e ->
        bind (go env e) (TEReturn $ TEVar $ FName "not") 
      FIsEmpty e ->
        bind (go env e) (TEVar $ FName "isEmptyG")

    eraseFun (TEFun f) = do
      x <- newName 
      TEAbs x <$> eraseFun (f (TEVar x))
    eraseFun (TEFun' n f) =
      TEAbs n <$> eraseFun (f (TEVar n))
    eraseFun (TEAbs n e) =
      TEAbs n <$> eraseFun e
    eraseFun (TEAbsT ns e) =
      TEAbsT ns <$> eraseFun e
    eraseFun (TEApp e1 e2) =
      TEApp <$> eraseFun e1 <*> eraseFun e2
    eraseFun (TEReturn e) =
      TEReturn <$> eraseFun e
    eraseFun (TEBind e1 e2) =
      TEBind <$> eraseFun e1 <*> eraseFun e2
    eraseFun (TEIf e1 e2 e3) =
      TEIf <$> eraseFun e1 <*> eraseFun e2 <*> eraseFun e3
    eraseFun (TEMConcat es) =
      TEMConcat <$> mapM eraseFun es
    eraseFun (TESequence es) =
      TESequence <$> mapM eraseFun es
    eraseFun (TEBin op e1 e2) =
      TEBin op <$> eraseFun e1 <*> eraseFun e2 
    eraseFun e = return e 

    isSimpleEnough (TEVar _) = True
    isSimpleEnough (TEReturn (TEVar _)) = True
    isSimpleEnough _ = False
                 
emitCode :: TargetExp -> Doc
emitCode e = go 0 e
  where
    parensIf b d = if b then parens d else d
    precApp   = 9
    precBind  = 1 
    precBinOp = 4 
    
    go v e =
      case e of
        TEEnd   -> text "end"
        TECons  -> text "consG"
        TEFix i | i == 1    -> text "fixG"
                | otherwise -> parensIf (v > precApp) $
                               text "gfixG" <+> text (show i)
        TEPara i | i == 1    -> text "paraG"
                 | otherwise -> parensIf (v > precApp) $
                                text "gparaG" <+> text (show i)
        TEDoc s -> parensIf (v > precApp) $ 
          text "docG" <+> text (show s)
        TELabel l ->
          case l of
            LConst s -> parensIf (v > precApp) $ 
                        text "LConst" <+> text (show s)
            LString s -> parensIf (v > precApp) $
                         text "LString" <+> text (show s)
            LInt n    -> parensIf (v > precApp) $
                         text "LInt" <+> text (show n)
            LBool b   -> parensIf (v > precApp)  $
                         text "LBool" <+> text (show b)
        TEAbs n (TEApp (TEProj i j) (TEVar m)) | n == m ->
          go v (TEProj i j)
        TEAbs n e ->
          let retrieveAbs (TEAbs n' e') =
                let (ns', e'') = retrieveAbs e'
                in (n':ns', e'')
              retrieveAbs e' = ([], e')
              (ns, e') = retrieveAbs (TEAbs n e)
          in parensIf (v > 0) $
             sep [text "\\" <+> hsep (map (text . show) $ ns) <+> text "->", 
                  nest 2 $ go 1 e']
        TEAbsT ns e ->
          parensIf (v > 0) $
          sep [ text "\\" <+> brackets (hsep $ punctuate comma $ map (text . show ) ns) <+> text "->",
                nest 2 $ go 1 e]
        TEApp (TEProj _ i) e2 ->
          parensIf (v > precBinOp) $
          hsep [ go (precBinOp+1) e2, text "!!", text (show i) ]
        TEApp e1 e2 ->
          parensIf (v > precApp) $
          hsep [ go precApp e1, go (precApp + 1) e2 ]
        TEReturn e  ->
          parensIf (v > precApp) $
          hsep [ text "return", go (precApp + 1) e ]
        TEBind e1 (TEAbs n (TEReturn e2)) ->
          parensIf (v > precApp) $
          hsep [text "fmap", sep [ go (precApp+1) (TEAbs n e2), go (precApp+1) e1 ]]
        TEBind e1 e2 ->
          parensIf (v > precBind) $
          hsep [ go precBind e1, text ">>=", go (precBind+1) e2 ]
        TEIf e1 e2 e3 ->
          parensIf (v > 0) $
          sep [ hsep [text "if", go 0 e1, text "then"],
                nest 2 (go 0 e2),
                text "else",
                nest 2 (go 0 e3) ]
        TEVar n -> text (show n)
        TEMConcat es -> -- union
          parensIf (v > precApp) $
          text "mconcat" <+>
          brackets (sep $ punctuate comma $ map (go 0) es)
        TESequence es -> -- tuple
          parensIf (v > precApp) $
          text "sequence" <+>
          brackets (sep $ punctuate comma $ map (go 0) es)
        TEProj _ i ->
          parens (text $ "!! " ++ show i)
        TEBin op e1 e2 ->
          parensIf (v > precBinOp) $ 
          hsep [ go (precBinOp+1) e1, text (show op), go (precBinOp+1) e2 ]
        TEBool b ->
          text (show b) 
          

    

          
ppr :: IFExp FTy -> Doc
ppr (Info t e) =
    case e of 
      FEmp        -> text "end"
      FCons e1 e2 -> 
          case e2 of 
            Info _ (FCons _ _) -> 
                pprP e1 <+> text "<:>" <+> ppr e2 
            _ -> 
                pprP e1 <+> text "<:>" <+> pprP e2 
      FUni e1 e2  -> 
          let retrieveUnion (Info _ (FUni e1 e2)) = e1:retrieveUnion e2 
              retrieveUnion e                     = [e]
              es2 = retrieveUnion e2 
          in text "unionsG" <+> brackets (sep (punctuate comma $ map ppr (e1:es2)))
      FTup []  -> text "return []"
      FTup es  -> sep [text "return",
                       nest 2 $ brackets (sep $ punctuate comma $ map ppr es)]
      FPrj _ i -> text "nthM " <> text (show i)
      FApp e1 e2 ->
          case info e2 of 
            FTyGraph _ -> 
                case e2 of 
                  Info _ (FVar y) -> 
                      pprP e1 <+> text (show y)
                  Info _ (FTup []) ->
                      pprP e1 <+> text "[]"
                  _ -> 
                      case e1 of 
                        Info _ (FPara i f) ->
                            if i == 1 then 
                                text "paraG" <+> pprP f <+> pprP e2 
                            else
                                text "gparaG" <+> text (show i) <+> pprP f <+> pprP e2 
                        _ -> 
                            pprP e1 <+> pprP e2 
                            -- sep [pprP e1 <+> text "=<<", nest 2 (pprP e2)]
            _ -> 
                pprP e1 <+> pprP e2 
      FAbsT ns e  ->
          parens $ sep [ text "\\" <+> brackets (hsep $ punctuate comma $ map (text . show ) ns) <+> text "->",
                         nest 2 $ ppr e]
      FAbs n e   ->
          let retrieveAbs (Info _ (FAbs n' e)) = 
                  let (ns,e') = retrieveAbs e 
                  in  (n':ns, e')
              retrieveAbs e = ([],e) 
              (ns,e') = retrieveAbs e 
          in parens $ sep [text "\\" <+> hsep (map (text . show) $ n:ns) <+> text "->", 
                           nest 2 $ ppr e']      
      FVar v ->
          case t of 
            -- FTyGraph 0 -> text "return []" 
            -- FTyGraph _ -> text "return" <+> text (show v)
            _          -> text (show v)
      FFix i v -> 
          if i == 1 then 
              text "fixG" <+> pprP v 
          else 
              text "gfixG" <+> text (show i) <+> pprP v 
      FPara i f -> 
          if i == 1 then 
              text "paraG" <+> pprP f 
          else 
              text "gparaG" <+> text (show i) <+> pprP f 
      FIf e1 e2 e3 -> 
          case info e1 of 
            FTyOther True -> 
                sep [ text "ifM", 
                      pprP e1, pprP e2, pprP e3 ]
            _  -> 
                sep [ text "if" <+> ppr e1 <+> text "then",
                      nest 2 (ppr e2),
                      text "else", 
                      nest 2 (ppr e3) ]
      FEq e1 e2 -> 
          case (info e1, info e2) of 
            (FTyOther False, FTyOther False) -> 
                pprP e1 <+> text "==" <+> pprP e2 
            _ ->
                text "liftM2 (==)" <+> tryLift (info e1) (pprP e1) <+> tryLift (info e2) (pprP e2)
      FAnd e1 e2 -> 
          case (info e1, info e2) of 
            (FTyOther False, FTyOther False) -> 
                pprP e1 <+> text "&&" <+> pprP e2 
            _ ->
                text "liftM2 (&&)" <+> tryLift (info e1) (pprP e1) <+> tryLift (info e2) (pprP e2)
      FOr  e1 e2 ->
          case (info e1, info e2) of 
            (FTyOther False, FTyOther False) -> 
                pprP e1 <+> text "||" <+> pprP e2 
            _ ->
                text "liftM2 (||)" <+> tryLift (info e1) (pprP e1) <+> tryLift (info e2) (pprP e2)
      FNot e1 -> 
          case info e1 of 
            FTyOther False -> 
                text "not" <+> pprP e1
            FTyOther True ->
                text "not =<<" <+> pprP e1 
      FLabel (LConst s) ->
          text "LConst" <+> text (show s) 
      FLabel (LString s) -> 
          text "LString" <+> text (show s) 
      FLabel (LInt n) ->
          text "LInt"    <+> text (show n) 
      FLabel (LBool n) -> 
          text "LBool"   <+> text (show n) 
      FBool b -> 
          text (show b) 
      FIsEmpty e -> 
          text "isEmptyG" <+> pprP e 
    where
      tryLift (FTyOther True)  b = b
      tryLift (FTyOther False) b = parens $ text "return" <+> b 

pprP :: Info (FExp FTy) FTy -> Doc
pprP (x@(Info t e)) =
    case e of 
      FEmp    -> ppr x 
      FBool _ -> ppr x 
      FAbs _ _ -> ppr x 
      FVar _  -> case t of -- FTyGraph _ -> parens (ppr x)
                           _          -> ppr x 
      _ -> parens (ppr x) 
      
      
      

            
                             
                   
    
