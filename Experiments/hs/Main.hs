module Main where

import Language.FUnCAL
import Language.FUnCAL.Graph 
import Util

import System.CPUTime
import System.IO

import Control.DeepSeq

import qualified C2O 
import qualified Class2RDB
import qualified PIM2PSM

import Control.Exception (evaluate)
import Criterion 
import Criterion.Main

readFromDot :: String -> IO (G IO UnCALLabel)
readFromDot filepath = do
  graph <- fromDotFile filepath 
  putStrLn $ "Read graph: " ++ show (vertexSize graph, edgeSize graph) ++ " from " ++ filepath 
  fromGraph graph 

evalAll :: (NFData a, Memoizable m) => G m a -> m ()
evalAll g = do 
  b <- isEmptyG =<< elimAll g
  let x = rnf b
  seq x (return x)
  where
    elimAll = foldG (\a r -> deepseq a (return r))
    

qBench :: NFData a => String -> G IO a -> (IO (G IO a) -> IO (G IO a)) -> Int -> IO Double
qBench msg input query n = do
  putStrLn "----------------------------------------------------" 
  putStrLn msg
  putStrLn "Loading input" 
  speedTest $ evalAll input
  putStrLn "Executing transformations"
  speedTests (const $ evalAll =<< query (return input)) [1..n]


tests :: IO () 
tests = do
  c2o_db       <- readFromDot "../db/customers_.dot"
  class2rdb_db <- readFromDot "../db/class6.dot"
  pim2psm_db   <- readFromDot "../db/PIM_db1.dot"
  aseq_db      <- readFromDot "../db/aseq30000.dot"
  alat_db      <- readFromDot "../db/lat200.dot"
  complete_db  <- readFromDot "../db/p200.dot"
  c2o_big_db   <- readFromDot "../db/customers_big.dot"
  c2o_big2_db  <- readFromDot "../db/customers_big_2000.dot"  
  c2o_big4_db  <- readFromDot "../db/customers_big_4000.dot"  

  evaluate =<< evalAll c2o_db
  evaluate =<< evalAll class2rdb_db
  evaluate =<< evalAll pim2psm_db 
  evaluate =<< evalAll aseq_db
  evaluate =<< evalAll alat_db
  evaluate =<< evalAll complete_db 
  evaluate =<< evalAll c2o_big_db
  evaluate =<< evalAll c2o_big2_db
  evaluate =<< evalAll c2o_big4_db 

  defaultMain [
    bench "C2O"       $ nfIO $ grun C2O.q       c2o_db, 
    bench "Class2RDB" $ nfIO $ grun Class2RDB.q class2rdb_db,
    bench "PIM2PSM"   $ nfIO $ grun PIM2PSM.q   pim2psm_db,
    bench "S30k"      $ nfIO $ grun a2d_xc      aseq_db,
    bench "M200"      $ nfIO $ grun a2d_xc      alat_db,
    bench "c200"      $ nfIO $ grun a2d_xc      complete_db,
    bench "C2O_1k"    $ nfIO $ grun C2O.q       c2o_big_db, 
    bench "C2O_2k"    $ nfIO $ grun C2O.q       c2o_big2_db,
    bench "C2O_4k"    $ nfIO $ grun C2O.q       c2o_big4_db
    ]
  where
    grun f g = evalAll =<< f (return g)


  -- sequence_ [ qBench "C2O"       c2o_db       C2O.q        500
  --           , qBench "Class2RDB" class2rdb_db Class2RDB.q  500
  --           , qBench "PIM2PSM"   pim2psm_db   PIM2PSM.q    500
  --           , qBench "S30k"      aseq_db      a2d_xc       5
  --           , qBench "M200"      alat_db      a2d_xc       5
  --           , qBench "c200"      complete_db  a2d_xc       5 ]

a2d_xc :: Memoizable m => m (G m UnCALLabel) -> m (G m UnCALLabel)
a2d_xc x = foldG (\a r -> if a == LConst "A" then
                            LConst "D" <:> return r
                          else
                            if a == LConst "C" then
                              return r
                            else
                              a <:> return r
                 ) =<< x 
main :: IO () 
main = tests
