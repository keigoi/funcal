Read graph: (42,45) from ../uncal/db/customers_.dot
Read graph: (70,73) from ../uncal/db/class6.dot
Read graph: (58,58) from ../uncal/db/PIM_db1.dot
Read graph: (30000,29999) from ../uncal/db/aseq30000.dot
Read graph: (40000,80000) from ../uncal/db/lat200.dot
Read graph: (201,40200) from ../uncal/db/p200.dot
benchmarking C2O
time                 1.431 ms   (1.415 ms .. 1.447 ms)
                     0.999 R²   (0.998 R² .. 0.999 R²)
mean                 1.406 ms   (1.393 ms .. 1.417 ms)
std dev              40.01 μs   (33.86 μs .. 48.50 μs)
variance introduced by outliers: 16% (moderately inflated)

benchmarking Class2RDB
time                 1.907 ms   (1.882 ms .. 1.934 ms)
                     0.997 R²   (0.995 R² .. 0.999 R²)
mean                 1.947 ms   (1.917 ms .. 1.999 ms)
std dev              134.9 μs   (86.39 μs .. 220.6 μs)
variance introduced by outliers: 52% (severely inflated)

benchmarking PIM2PSM
time                 1.994 ms   (1.976 ms .. 2.012 ms)
                     0.999 R²   (0.999 R² .. 1.000 R²)
mean                 2.013 ms   (1.999 ms .. 2.029 ms)
std dev              50.02 μs   (39.32 μs .. 66.46 μs)
variance introduced by outliers: 12% (moderately inflated)

benchmarking S30k
time                 462.3 ms   (NaN s .. 577.9 ms)
                     0.984 R²   (0.946 R² .. 1.000 R²)
mean                 474.7 ms   (450.2 ms .. 493.0 ms)
std dev              27.92 ms   (0.0 s .. 31.69 ms)
variance introduced by outliers: 19% (moderately inflated)

benchmarking M200
time                 1.670 s    (1.634 s .. 1.719 s)
                     1.000 R²   (1.000 R² .. 1.000 R²)
mean                 1.618 s    (1.602 s .. 1.630 s)
std dev              19.13 ms   (0.0 s .. 21.43 ms)
variance introduced by outliers: 19% (moderately inflated)

benchmarking c200
time                 1.030 s    (996.8 ms .. 1.068 s)
                     1.000 R²   (0.999 R² .. 1.000 R²)
mean                 1.000 s    (980.4 ms .. 1.010 s)
std dev              17.08 ms   (0.0 s .. 17.29 ms)
variance introduced by outliers: 19% (moderately inflated)

