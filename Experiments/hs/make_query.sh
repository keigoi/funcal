#!/bin/sh 

CONVERTER_DIR="../../UnCAL2FUnCAL"
CONVERTER="$CONVERTER_DIR/Main"

function insert_template() {
    FILE=$1
    echo "" >> $FILE
    echo "import Language.FUnCAL" >> $FILE
    echo "import Util"            >> $FILE
    echo ""                       >> $FILE
    echo "q xdb = "               >> $FILE
}


make -C $CONVERTER_DIR

# Making PIM2PSM 
echo "Generating PIM2PSM.hs ..."

echo "module PIM2PSM where" > ./PIM2PSM.hs 
insert_template ./PIM2PSM.hs 
$CONVERTER ../uncal/query/PIM2PSM1.uncal >> ./PIM2PSM.hs 

# Making C2O 
echo "Generating C2O.hs ..."

echo "module C2O where" > ./C2O.hs
insert_template ./C2O.hs
$CONVERTER ../uncal/query/c2o_select_demo.uncal >> ./C2O.hs

# Making Class2RDB
echo "Generating Class2RDB.hs ..."

echo "module Class2RDB where" > ./Class2RDB.hs
insert_template ./Class2RDB.hs 
$CONVERTER ../uncal/query/class2rdb6.uncal >> ./Class2RDB.hs 



