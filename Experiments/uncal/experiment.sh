CMD=./run_experiment_uncal.sh 
TIMEOUT=gtimeout
DUR=60

function bench() {
    ITER=$1 
    NAME=$2

    FILEB="./res/${NAME}_bulk.txt"
    FILER="./res/${NAME}_rec.txt"

    FILEBT="./res/${NAME}_bulk_ht.txt"
    FILERT="./res/${NAME}_rec_ht.txt"

    echo "Executing ${NAME} ..."
    echo "--- ${ITER} --------------" >> $FILEB
    $TIMEOUT $DUR $CMD $NAME          >> $FILEB

    echo "Executing ${NAME} -rec ..."
    echo "--- ${ITER} --------------" >> $FILER
    $TIMEOUT $DUR $CMD $NAME -rec     >> $FILER 

    echo "Executing ${NAME} -ht ..."
    echo "--- ${ITER} --------------" >> $FILEBT
    $TIMEOUT $DUR $CMD $NAME      -ht >> $FILEBT   

    echo "Executing ${NAME} -rec -ht ..."
    echo "--- ${ITER} --------------" >> $FILERT
    $TIMEOUT $DUR $CMD $NAME -rec -ht >> $FILERT
}

for i in `seq 1 5`; do 
    echo "Iteration: $i"
    bench $i "pim2psm"
    bench $i "class2rdb"
    bench $i "c2o"
done 

for j in `seq 1 5`; do 
    echo "Iteration: $j"
    bench $j "s30k"
    bench $j "m200"
    bench $j "c200"
done
