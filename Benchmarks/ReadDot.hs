import Language.FUnCAL
import Language.FUnCAL.Graph 
import Data.Time.Clock 

import Data.List (delete)
import Control.DeepSeq
import qualified Data.Vector as V


import qualified Data.IntMap as IM

evalAll :: (NFData a, Memoizable m) => G m a -> m ()
evalAll g = do 
  b <- isEmptyG =<< elimAll g
  let x = rnf b
  seq x (return x)
  where
    elimAll = foldG (\a r -> deepseq a (return r))

a2d_xc = foldG (\a r -> if a == LConst "A" then
                          LConst "D" <:> return r
                        else
                          if a == LConst "C" then
                            return r
                          else
                            a <:> return r
               )

lat = V.head <$> 
        gfixG 40000 (\xs ->
                      V.generateM 40000 (\i ->
                        mconcat [LConst "A" <:> return (xs V.! ((i+200)`mod`40000))
                                ,LConst "A" <:> return (xs V.! ((i+1)`mod`40000))]
                                        ))
         
main = do
--  alat_db <- fromGraph =<< fromDotFile "./Experiments/db/lat200.dot"
  start <- getCurrentTime
  res <- a2d_xc =<< lat
  evalAll res
  g <- toGraph res
  print (vertexSize g, edgeSize g)
  end   <- getCurrentTime 
  putStrLn $ "Reading dot takes: " ++ show (diffUTCTime end start)
